#include <stdio.h>
#include <stdarg.h>
#include "nsu.h"

int nsu_log_level = NSU_LOG_ERR;

static char const *nsu_severity_str[] = {
	"debug",
	"warning",
	"error",
};

static void
__nsu_log_prt(int severity, char const *fmt, va_list ap)
{
	if (severity >= 0 &&
	    severity < sizeof(nsu_severity_str)/sizeof(nsu_severity_str[0])) {
	    fprintf(stderr, "nsu %s: ", nsu_severity_str[severity]);
	    vfprintf(stderr, fmt, ap);
	    fputc('\n', stderr);
	}
}

void (*nsu_logger)(int, char const *, va_list) = __nsu_log_prt;

void
nsu_log_msg(int severity, char const *fmt, ...)
{
	if (nsu_logger) {
		va_list ap;
		va_start(ap, fmt);
		nsu_logger(severity, fmt, ap);
		va_end(ap);
	}
}

