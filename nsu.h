#include <resolv.h>
#include <stdarg.h>

enum {
	NSU_PREREQ_YXRRSET,   /* RRset exists (value independent) */
	NSU_PREREQ_YXRRSETV,  /* RRset exists (value dependent) */
	NSU_PREREQ_NXRRSET,   /* RRset does not exist */
	NSU_PREREQ_YXDOMAIN,  /* Name is in use */
	NSU_PREREQ_NXDOMAIN   /* RRset does not exist */
};

enum {
	NSU_UPDATE_ADD,    /* Add to an RRset */
	NSU_UPDATE_DEL,    /* Delete an RRset */
	NSU_UPDATE_DELV,   /* Delete an RR from an RRset */
	NSU_UPDATE_DELALL  /* Delete all RRsets from a name */
};

typedef struct __nsu_rr {
	int op; /* One of the above codes */
	int type; /* RR type */
	char name[NS_MAXDNAME];
	int ttl;
	union __nsu_rdata {
		struct in_addr d_a;     /* ns_t_a */
		char *d_name;           /* ns_t_ns, ns_t_cname, ns_t_ptr */
		char *d_txt;            /* ns_t_txt */
		char *d_x25;            /* ns_t_x25 */
		struct {
			unsigned short pref;
			char *name;
		} d_mx;                 /* ns_t_mx */
#if 0
		struct in6_addr d_aaaa; /* ns_t_aaaa */
		struct {
			uint16_t priority;
			uint16_t weight;
			uint16_t port;
			char *target;
		} d_srv;                /* ts_t_srv */
#endif
		struct {
			char *cpu;
			char *os;
		} d_hinfo;
		struct {
			char *rmailbx;
			char *emailbx;
		} d_minfo;
		struct {
			char *mname;
			char *rname;
			uint32_t serial, refresh, retry, expire, minimum;
		} d_soa;
		struct {
			uint32_t address;
			uint8_t protocol;
			size_t bitlen;
			uint8_t *bitmap;
		} d_wks;
		struct {
			char *mbox_dname;
			char *txt_dname;
		} d_rp;
		struct {
			uint16_t priority;
			uint16_t weight;
			uint16_t port;
			char *target;
		} d_srv;
		struct {
			uint16_t rdlength;
			u_char *rdata;
		} d_any;                /* Any other type */
	} data;
} nsu_rr;

typedef struct {
	unsigned        id :16;         /*%< query identification number */
#if BYTE_ORDER == BIG_ENDIAN
	/* fields in third byte */
	unsigned        qr: 1;          /*%< response flag */
	unsigned        opcode: 4;      /*%< purpose of message */
	unsigned        z0: 3;          /*%< reserved */
	/* fields in fourth byte */
	unsigned        z1: 4;          /*%< reserved */
	unsigned        rcode :4;       /*%< response code */
#else
	/* fields in third byte */
	unsigned        z0: 3;          /*%< reserved */
	unsigned        opcode: 4;      /*%< purpose of message */
	unsigned        qr: 1;          /*%< response flag */
	/* fields in fourth byte */
	unsigned        rcode :4;       /*%< response code */
	unsigned        z1: 4;          /*%< reserved */
#endif
	/* remaining bytes */
	unsigned        zocount: 16;    /*%< number of RRs in the Zone Section */
	unsigned        prcount: 16;    /*%< number of RRs in the Prerequisite Section */
	unsigned        upcount: 16;    /*%< number of RRs in the Update Section. */
	unsigned        adcount: 16;    /*%< number of RRs in the Additional Data Section */
} UPDATE_HEADER;

struct nsu_tsig_alg;

struct nsu_tsig {
	char *name;
	struct nsu_tsig_alg const *alg;
	unsigned short fudge;
	char *ctx;
	unsigned char *digest;
};

struct nsu_tsig_alg const *nsu_tsig_alg_find_code(int code);
struct nsu_tsig_alg const *nsu_tsig_alg_find_name(char const *name);

char const *nsu_tsig_alg_name(struct nsu_tsig_alg const *alg);
size_t nsu_tsig_alg_digest_size(struct nsu_tsig_alg const *alg);

struct nsu_tsig *nsu_tsig_create(struct nsu_tsig_alg const *alg, char const *keyname);
void nsu_tsig_free(struct nsu_tsig *sgn);

void nsu_tsig_set_key(struct nsu_tsig *sgn, u_char const *key, size_t len);
void nsu_tsig_update(struct nsu_tsig *sgn, u_char const *data, size_t len);
void nsu_tsig_digest(struct nsu_tsig *sgn, u_char *data, size_t len);

int nsu_str_to_type(char const *str);
char const *nsu_type_to_str(int type);

int nsu_nmkquery(res_state statp,
		 char const *zone,
		 nsu_rr *prereq, int num_prereq,
		 nsu_rr *update, int num_update,
		 struct nsu_tsig *tsig,
		 u_char *buf, int buflen);
int nsu_mkquery(char const *zone,
		nsu_rr *prereq, int num_prereq,
		nsu_rr *update, int num_update,
		struct nsu_tsig *tsig,
		u_char *buf, int buflen);

enum {
	NSU_LOG_DEBUG,
	NSU_LOG_WARN,
	NSU_LOG_ERR,
	NSU_LOG_NONE
};

extern int nsu_log_level;
extern void (*nsu_logger)(int, char const *, va_list);

void nsu_log_msg(int severity, char const *fmt, ...);

#define nsu_log_enabled(svr) ((svr) >= nsu_log_level)

#define nsu_log(svr, fmt, ...)						\
	do {								\
		if (nsu_log_enabled(svr)) {				\
			nsu_log_msg(svr, fmt, __VA_ARGS__);		\
		}							\
	} while (0)

enum {
	NSU_KEYFILE_OK,
	NSU_KEYFILE_BAD,
	NSU_KEYFILE_ALG,
	NSU_KEYFILE_ERR
};

int nsu_keyfile_key_read(char const *filename,
			 struct nsu_tsig_alg const **palg,
			 char **pkeyname,
			 uint8_t **pkey, size_t *pkey_len);
int nsu_keyfile_private_read(char const *filename,
			     struct nsu_tsig_alg const **palg,
			     char **pkeyname,
			     uint8_t **pkey, size_t *pkey_len);

int nsu_keyfile_read(char const *name, struct nsu_tsig **ptsig);

typedef struct {
	socklen_t len;
	union {
		struct sockaddr sa;
		struct sockaddr_in in4;
		struct sockaddr_in6 in6;
	} addr;
} nsu_sockaddr;

enum {
	NSU_SEND_OK,
	NSU_SEND_SYSERR,
	NSU_SEND_TIMEOUT,
	NSU_SEND_BADSERV,
	NSU_SEND_RETRYTCP,
	NSU_SEND_PEERCLOSE
};

int nsu_nsend(res_state statp, nsu_sockaddr *a, u_char *qbuf, size_t qlen,
	      u_char *rbuf, size_t rlen, size_t *return_len);



