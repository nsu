#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <errno.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <nettle/md5.h>
#include <nettle/sha1.h>
#include <nettle/sha2.h>
#include <nettle/hmac.h>
#include <nettle/base64.h>
#include "nsu.h"
#include "nsu_app.h"

char const *progname;
unsigned long default_ttl = 600;
char const *domain_name;
struct sockaddr_in *server_addr;
char const domain_service[] = "53";
struct nsu_tsig *tsig;

void
vdiagprt(char const *fmt, va_list ap)
{
	fprintf(stderr, "%s: ", progname);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
}

void
panic(char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vdiagprt(fmt, ap);
	va_end(ap);
}

void
abend(char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vdiagprt(fmt, ap);
	va_end(ap);
	exit(1);
}

void
abend_nomem(void)
{
	abend("not enough memory");
}

#define MAX_NS 20

static void
ns_perror(char const *name, int verbose)
{
	fprintf(stderr, "%s: %s: ", progname, name);
	fputc('\n', stderr);
}

static char *ns_rcode_strtab[] = {
	[ns_r_noerror] = "No error occurred.",
        [ns_r_formerr] = "Format error.",
        [ns_r_servfail] = "Server failure.",
        [ns_r_nxdomain] = "Name error.",
        [ns_r_notimpl] = "Unimplemented.",
        [ns_r_refused] = "Operation refused.",
        [ns_r_yxdomain] = "Name exists.",
        [ns_r_yxrrset] = "RRset exists.",
        [ns_r_nxrrset] = "RRset does not exist.",
        [ns_r_notauth] = "Not authoritative for zone.",
        [ns_r_notzone] = "Zone of record different from zone section.",
        [ns_r_max] = "ns_r_max",
        [ns_r_badvers] = "ns_r_badvers",
        [ns_r_badsig] = "Bad signature.",
        [ns_r_badkey] = "Bad key.",
        [ns_r_badtime] = "Bad time."
};

char const *
ns_rcodestr(int code)
{
	if (code >= 0 &&
	    code < sizeof(ns_rcode_strtab)/sizeof(ns_rcode_strtab[0]) &&
	    ns_rcode_strtab[code])
		return ns_rcode_strtab[code];
	return "Unknown response code";
}

void
nstab_add(ns_msg handle, ns_rr rr, struct sockaddr_in *nstab, int *nsnum)
{
	char name[MAXDNAME];
	struct addrinfo hints, *addr, *ap;
	
	if (ns_name_uncompress(ns_msg_base(handle),
			       ns_msg_end(handle),
			       ns_rr_rdata(rr),
			       name,
			       sizeof(name)) < 0) {
		abend("ns_name_uncompress failed");
	}

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;

	if (getaddrinfo(name, domain_service, &hints, &addr)) {
		abend("%s: invalid IP address", name);
	}
	
	for (ap = addr; ap && *nsnum < MAX_NS; ap = ap->ai_next) {
		if (ap->ai_addrlen == sizeof(struct sockaddr_in)) {
			int i;

			for (i = 0; i < *nsnum; i++) {
				if (!memcmp(&nstab[i], ap->ai_addr,
					    ap->ai_addrlen)) {
					goto next;
				}
			}
			memcpy(&nstab[*nsnum], ap->ai_addr, ap->ai_addrlen);
			++ *nsnum;
		}
	next:
		continue;
	}
	freeaddrinfo(addr);
}

void
ns_add(struct sockaddr_in *nstab, int *nsnum, ns_msg handle, ns_sect section)
{
	int rrnum;	  /* resource record number */

	for (rrnum = 0; rrnum < ns_msg_count(handle, section); rrnum++) {
		ns_rr rr;

		if (*nsnum == MAX_NS)
			break;

		if (ns_parserr(&handle, section, rrnum, &rr)) {
			panic("ns_parserr: %s", strerror(errno));
		}

		if (ns_rr_type(rr) == ns_t_ns) {
			nstab_add(handle, rr, nstab, nsnum);
		}
	}
}

typedef union {
	HEADER hdr;
	u_char buf[4096];
} QUERYBUF;


void
ns_add_master(char const *domain, struct sockaddr_in *nstab, int *nsnum)
{
	QUERYBUF response, query;
	int rlen, qlen;
	ns_msg handle;
	ns_rr rr;
	char name[MAXDNAME];
	
	qlen = res_mkquery(ns_o_query,
			   domain,
			   ns_c_in,
			   ns_t_soa,
			   NULL,
			   0,
			   NULL,
			   query.buf,
			   sizeof(query));

	rlen = res_send(query.buf, qlen,
			response.buf, sizeof(response));

	if (rlen < 0) {
		if (errno == ECONNREFUSED) {	/* no server on the host */
			panic("no name servers configured");
		} else {
			/* anything else: no response */
			panic("no response");
		}
		return;
	}

	if (ns_initparse(response.buf, rlen, &handle) < 0) {
		panic("ns_initparse: %s", strerror(errno));
		return;
	}

	if (ns_msg_getflag(handle, ns_f_rcode) != ns_r_noerror) {
		panic("%s: %s", domain,
		       ns_rcodestr(ns_msg_getflag(handle, ns_f_rcode)));
		return;
	}

	if (ns_msg_count(handle, ns_s_an) != 1) {
		panic("%s: expected 1 answer, got %d",
		      domain, ns_msg_count(handle, ns_s_an));
		return;
	}

	if (ns_parserr(&handle, ns_s_an, 0, &rr)) {
		if (errno != ENODEV) {
			panic("ns_parserr: %s", strerror(errno));
		}
	}

	if (ns_rr_type(rr) != ns_t_soa) {
		panic("expected answer type %d, got %d",
		      ns_t_soa, ns_rr_type(rr));
		return;
	}

	nstab_add(handle, rr, nstab, nsnum);
}

static int
find_ns(char const *domain, struct sockaddr_in *nstab, int verbose)
{
	int nscount = 0;
	QUERYBUF response; /* response buffers */
	int resplen; /* buffer length */
	ns_msg handle;

	resplen = res_query(domain, ns_c_in, ns_t_ns,
			    response.buf, sizeof(response));
	if (resplen < 0) {
		switch (h_errno) {
		case HOST_NOT_FOUND:
			if (verbose)
				panic("%s: host not found", domain);
			return 0;

		case NO_DATA:
			if (verbose)
				panic("%s: no NS records", domain);
			return 0;

		case TRY_AGAIN:
			panic("no response for DNS query");
			break;

		default:
			panic("%s: unexpected error (%d)", h_errno);
		}
		return -1;
	}

	if (ns_initparse(response.buf, resplen, &handle) < 0) {
		panic("ns_initparse: %s", strerror(errno));
		return -1;
	}

	ns_add_master(domain, nstab, &nscount);
	ns_add(nstab, &nscount, handle, ns_s_an);
	ns_add(nstab, &nscount, handle, ns_s_ns);

	return nscount;
}

static void
set_server(char *name)
{
	struct addrinfo hints, *r;
	char *service;
	struct __res_state *st = &_res;

	service = strchr(name, ':');
	if (service)
		*service++ = 0;
	else
		service = (char*)domain_service;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;

	if (getaddrinfo(name, service, &hints, &r)
	    || r->ai_addrlen != sizeof(struct sockaddr_in)) {
		abend("%s: invalid IP address", name);
	}
	server_addr = malloc(r->ai_addrlen);
	if (!server_addr)
		abend_nomem();
	memcpy(server_addr, r->ai_addr, r->ai_addrlen);
	memcpy(&_res.nsaddr_list[0], r->ai_addr, r->ai_addrlen);
	_res.nscount = 1;
	freeaddrinfo(r);
}

static int
get_arg_ul(char const *arg, unsigned long *np)
{
	char *p;
	unsigned long n;
	errno = 0;
	n = strtoul(arg, &p, 10);
	if (errno || *p)
		return -1;
	*np = n;
	return 0;
}

/*
 * nsu
 */
int
main(int argc, char **argv)
{
	char *input_file = NULL;
	char *keyfile_name = NULL;
	int i;
	nsu_rr upd;
	char *p;

	progname = argv[0];
	res_init();
	yy_flex_debug = 0;
	while ((i = getopt(argc, argv, "d:f:k:s:Tt:x")) != EOF) {
		switch (i) {
		case 'd':
			domain_name = optarg;
			break;
			
		case 'f':
			input_file = optarg;
			break;

		case 'k':
			keyfile_name = optarg;
			break;
			
		case 's':
			set_server(optarg);
			break;

		case 'T':
			rdata_list();
			return 0;
			
		case 't':
			if (get_arg_ul(optarg, &default_ttl))
				exit(1);
			break;

		case 'x':
			if (yydebug == 1)
				yy_flex_debug = 1;
			else if (nsu_log_level == NSU_LOG_DEBUG)
				yydebug = 1;
			else
				nsu_log_level = NSU_LOG_DEBUG;
			break;
			
		default:
			exit(1);
		}
	}

	argc -= optind;
	argv += optind;

	if (keyfile_name) {
		switch (nsu_keyfile_read(keyfile_name, &tsig)) {
		case NSU_KEYFILE_OK:
			break;
			
		case NSU_KEYFILE_BAD:
			abend("bad keyfile format");
			
		case NSU_KEYFILE_ALG:
			abend("unknown or unsupported algorithm in keyfile");
			
		case NSU_KEYFILE_ERR:
			abend("%s: %s", keyfile_name, strerror(errno));

		default:
			abort();
		}
	}
	
	if (input_file)
		lexer_from_file(input_file);
	else if (argc > 0)
		lexer_from_argv(argc, argv);
	else
		lexer_from_file("-");
	yyparse();
	exit(0);
}

enum {
	update_fail = -1,
	update_ok,
	update_retry
};

static char const *
nsu_sockaddr_str(nsu_sockaddr *nsa, char *name, size_t namelen)
{
	getnameinfo(&nsa->addr.sa, nsa->len,
		    name, namelen,
		    NULL, 0,
		    NI_NUMERICHOST);
	return name;
}

int
send_update_query(res_state state, nsu_sockaddr *nsa,
		  char const *zone, u_char *qbuf, int qlen)
{
	union {
		UPDATE_HEADER hdr;
		u_char buf[NS_PACKETSZ];
	} response;
	size_t rlen;
	ns_msg handle;
	int rcode;
	char name[NS_MAXDNAME];
	char const *nsname = NULL;
	int rc;
	
	if (nsu_log_enabled(NSU_LOG_DEBUG) || state->options & RES_DEBUG) {
		nsname = nsu_sockaddr_str(nsa, name, sizeof(name));
		nsu_log_msg(NSU_LOG_DEBUG, "trying nameserver %s", name);
	}

	rc = nsu_nsend(state, nsa, qbuf, qlen,
		       (u_char *) &response,
		       sizeof(response),
		       &rlen);

	if (rc == NSU_SEND_OK) {
		
	} else {
		if (!nsname)
			nsname = nsu_sockaddr_str(nsa, name, sizeof(name));
		switch (rc) {
		case NSU_SEND_SYSERR:
			panic("%s: can't send: %s", nsname, strerror(errno));
			break;
				
		case NSU_SEND_TIMEOUT:
			panic("%s: server timeout", nsname);
			break;

		case NSU_SEND_BADSERV:
			panic("%s: response from another server", nsname);
			break;
			
		case NSU_SEND_RETRYTCP:
			panic("%s: response too long; retry using TCP",
			      nsname);
			break;
			
		case NSU_SEND_PEERCLOSE:
			panic("%s: server closed connection", nsname);
			break;

		default:
			abort();
		}
		return update_fail;
	}

	if (ns_initparse(response.buf, rlen, &handle) < 0) {
		panic("ns_initparse: %s", strerror(errno));
		return update_fail;
	}

	rcode = ns_msg_getflag(handle, ns_f_rcode);
	switch (rcode) {
		/*
		 * If a response is received whose RCODE is SERVFAIL or
		 * NOTIMP, or if no response is received within an
		 * implementation dependent timeout period, or if an ICMP
		 * error is received indicating that the server's port is
		 * unreachable, then the requestor will delete the unusable
		 * server from its internal name server list and try the
		 * next one, repeating until the name server list is empty.
		 * If the requestor runs out of servers to try, an
		 * appropriate error will be returned to the requestor's
		 * caller.
		 */
	case ns_r_notimpl:
	case ns_r_servfail:
		if (nsu_log_enabled(NSU_LOG_DEBUG)
		    || state->options & RES_DEBUG) {
			char name[NS_MAXDNAME];
			
			getnameinfo(&nsa->addr.sa, nsa->len,
				    name, sizeof(name),
				    NULL, 0,
				    NI_NUMERICHOST);
			nsu_log_msg(NSU_LOG_DEBUG, "nameserver %s: %s", name,
				    ns_rcodestr(rcode));
		}
		return update_retry;
		
	case ns_r_noerror:
		break;
		
	default:
		panic("%s: %s", zone, ns_rcodestr(rcode));
		return update_fail;
	}
	return update_ok;
}

int
run_update(nsu_rr *prereq, int num_prereq,
	   nsu_rr *update, int num_update)
{
	struct sockaddr_in nstab[MAX_NS];
	int nscount = 0;
	int qlen;
	QUERYBUF query;
	char const *zone = NULL;
	int rc;
	int i;
	struct __res_state rstate;
	nsu_sockaddr addr;
	
	if (!domain_name) {
		char *p = update->name;

		while (1) {
			nscount = find_ns(p, nstab, 0);
			if (nscount > 0) {
				zone = p;
				break;
			}
			if ((p = strchr(p, '.')) == NULL)
				break;
			p++;

		}
		if (!zone)
			abend("cannot determine nameservers for update");
	} else {
		zone = domain_name;
		nscount = find_ns(zone, nstab, 1);
		if (nscount == 0)
			return -1;
	}

#if 0
	/* Override the name servers */
	if (server_addr) {
		memcpy(nstab, server_addr, sizeof(nstab[0]));
		nscount = 1;
	}
#endif
	qlen = nsu_mkquery(zone,
			   prereq, num_prereq,
			   update, num_update,
			   tsig,
			   query.buf, sizeof(query.buf));
	
	if (qlen < 0)
		abend("failed to create update query");


	res_ninit(&rstate);
	addr.len = sizeof(struct sockaddr_in);
	for (i = 0; i < nscount; i++) {
		memcpy(&addr.addr, &nstab[i],  sizeof(nstab[0]));
		rc = send_update_query(&rstate, &addr, zone, query.buf, qlen);
		if (rc != update_retry)
			break;
	}
	return rc == update_ok ? 0 : -1;
}
