%{
#include <stdlib.h>
#include <string.h>
#include "nsu.h"
#include "nsu_app.h"

#define MAX_RR 128

static nsu_rr rr_base[MAX_RR];
static int rr_count = 0;
static int update_start = -1;

static nsu_rr *
alloc_rr(int op, char *name, nsu_locus *loc)
{
	nsu_rr *rr;
	
	if (rr_count == MAX_RR) {
		error_at_locus(loc, "too many RRs");
		return NULL;
	}
	if (strlen(name) + 1 > sizeof(rr->name)) {
		error_at_locus(loc, "name too long");
		return NULL;
	}
	rr = &rr_base[rr_count++];
	memset(rr, 0, sizeof *rr);
	rr->op = op;
	strcpy(rr->name, name);
	return rr;
}

%}

%error-verbose
%locations

%token T_NXDOMAIN   "nxdomain"
%token T_YXDOMAIN   "yxdomain"
%token T_NXRRSET    "nxrrset"
%token T_YXRRSET    "yxrrset"
%token T_ADD        "add"
%token T_DELETE     "delete"
%token T_SEND       "send"
%token T_UPDATE     "update"
%token T_EOL        "end of statement"
%token <s> T_STRING "string"
%token <n> T_NUMBER "number"
%token <r> T_TYPEX  "#\\ N HEXDATA"

%type <s> domain
%type <n> ttl type
%type <r> rdata

%union {
	char *s;
	unsigned long n;
	nsu_rr r;
}

%%

input       : stmt
            | input stmt
            | error T_EOL
              {
		      dealloc_strings();
		      yyclearin;
		      yyerrok;
	      }
            | input error T_EOL
              {
		      dealloc_strings();
		      yyclearin;
		      yyerrok;
	      }
            ;

stmt        : opt_prereq_list { update_start = rr_count; } update_stmt
              {
		      run_update(rr_base, update_start,
				 rr_base + update_start,
				 rr_count - update_start);
		      dealloc_strings();
		      rr_count = 0;
	      }
            | T_EOL
            | T_SEND T_EOL
	    ;

opt_prereq_list : /* empty */
            | prereq_list
	    ;

prereq_list : prereq 
            | prereq_list prereq
            ;

prereq      : prereq_cond T_EOL
            ;

prereq_cond : T_NXDOMAIN domain
              {
		      if (!alloc_rr(NSU_PREREQ_NXDOMAIN, $2, &@2))
			      YYERROR;
	      }
            | T_YXDOMAIN domain
              {
		      if (!alloc_rr(NSU_PREREQ_YXDOMAIN, $2, &@2))
			      YYERROR;
	      }
            | T_NXRRSET domain type
	      {
		      nsu_rr *rr = alloc_rr(NSU_PREREQ_NXRRSET, $2, &@2);
		      if (!rr)
			      YYERROR;
		      rr->type = $3;
	      }
            | T_YXRRSET domain type rdata
	      {
		      nsu_rr *rr = alloc_rr(NSU_PREREQ_YXRRSET, $2, &@2);
		      if (!rr)
			      YYERROR;
		      rr->type = $3;
		      if ($4.type == $3)
			      memcpy(&rr->data, &$4.data, sizeof(rr->data));
	      }
            ;

rdata       : /* emtpy */
              {
		      rdata_parse(&$$, $<n>0);
	      }
            ;

update_stmt : update
            | update_stmt update
            ;

update      : update_expr T_EOL
            | T_UPDATE update_expr T_EOL
            ;

update_expr : T_DELETE domain
              {
		      if (!alloc_rr(NSU_UPDATE_DELALL, $2, &@2))
			      YYERROR;
	      }
            | T_DELETE domain type rdata
	      {
		      nsu_rr *rr = alloc_rr($3 == $4.type ?
					    NSU_UPDATE_DELV : NSU_UPDATE_DEL,
					    $2, &@2);
		      if (!rr)
			      YYERROR;
		      rr->type = $3;
		      if (rr->op == NSU_UPDATE_DELV)
			      memcpy(&rr->data, &$4.data, sizeof(rr->data));
	      }
            | T_ADD domain ttl type rdata
	      {
		      nsu_rr *rr;
		      
		      if ($4 != $5.type) {
			      yyerror("need RDATA");
			      YYERROR;
		      }
		      rr = alloc_rr(NSU_UPDATE_ADD, $2, &@2);
		      if (!rr)
			      YYERROR;
		      rr->ttl = $3;
		      rr->type = $4;
		      memcpy(&rr->data, &$5.data, sizeof(rr->data));
	      }
            ;

domain      : { need_domain(); } T_STRING
              {
		      $$ = $2;
	      }
            ;

ttl         : /* empty */
              {
		      $$ = default_ttl;
	      }
            | T_NUMBER
	    ;

type        : T_STRING
              {
		      $$ = nsu_str_to_type($1);
		      if ($$ == ns_t_invalid) {
			      error_at_locus(&@1,
					     "%s: invalid or unsupported type",
					     $1);
			      YYERROR;
		      }
              }
            ;
%%

int
yyerror(const char *msg)
{
	error_at_locus(NULL, "%s", msg);
}

