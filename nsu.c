#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <errno.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include "nsu.h"

#define PUT_STRING(cp, buflen, s, n)				\
	do {							\
		if (buflen < n + 1)				\
			return -1;				\
		else {						\
		        *cp++ = n;				\
			memcpy(cp, s, n);			\
			cp += n;				\
			buflen -= n + 1;			\
		}						\
	} while (0)

#define PUT_STRINGZ(cp, buflen, s)			\
	do {						\
	        size_t __n = strlen(s);			\
		PUT_STRING(cp, buflen, s, __n);		\
	} while (0)

static int
nsu_put_data(nsu_rr *updr, u_char *cp, int buflen,
	     const u_char **dnptrs, const u_char **lastdnptr)
{
	int n, len;
	u_char *start;
	
	switch (updr->type) {
	case ns_t_txt: {
		char *p = updr->data.d_txt;
		n = strlen(p);
		len = n + (n + 254) / 255;
		if (buflen < len)
			return -1;
		while (n > 0) {
			int slen = n;
			if (slen > 255)
				slen = 255;

			*cp++ = slen;
			memcpy(cp, p, slen);

			cp += slen;
			
			p += slen;
			n -= slen;
		}
		break;
	}
		
	case ns_t_a:
		len = 4;
		if (buflen < len)
			return -1;
		memcpy(cp, &updr->data.d_a, len);
		break;
			
	case ns_t_ns:
	case ns_t_cname:
	case ns_t_ptr:
	case ns_t_mb:
	case ns_t_md:
	case ns_t_mf:
	case ns_t_mg:
	case ns_t_mr:
		n = ns_name_compress(updr->data.d_name, cp, buflen,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		len = n;
		break;

	case ns_t_dname:
		/*
		 * RFC 6672: [The DNAME's] RDATA is comprised of a single
		 * field, <target>, which contains a fully qualified domain
		 * name that MUST be sent in uncompressed form
		 */
		n = ns_name_compress(updr->data.d_name, cp, buflen,
				     NULL, NULL);
		if (n < 0)
			return -1;
		len = n;
		break;		
		
	case ns_t_mx:
		if (buflen < 2)
			return -1;
		NS_PUT16(updr->data.d_mx.pref, cp);
		n = ns_name_compress(updr->data.d_mx.name,
				     cp, buflen - 2,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		len = n + 2;
		break;

	case ns_t_hinfo:
		start = cp;
		PUT_STRINGZ(cp, buflen, updr->data.d_hinfo.cpu);
		PUT_STRINGZ(cp, buflen, updr->data.d_hinfo.os);
		len = cp - start;
		break;

	case ns_t_minfo:
		start = cp;
		n = ns_name_compress(updr->data.d_minfo.rmailbx,
				     cp, buflen,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;
		
		n = ns_name_compress(updr->data.d_minfo.emailbx,
				     cp, buflen,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;
		len = cp - start;
		break;		

	case ns_t_soa:
		start = cp;

		n = ns_name_compress(updr->data.d_soa.mname,
				     cp, buflen,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;

		n = ns_name_compress(updr->data.d_soa.rname,
				     cp, buflen,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;

		if (buflen < sizeof(uint32_t) * 5)
			return -1;

		NS_PUT32(updr->data.d_soa.serial, cp);
		NS_PUT32(updr->data.d_soa.refresh, cp);
		NS_PUT32(updr->data.d_soa.retry, cp);
		NS_PUT32(updr->data.d_soa.expire, cp);
		NS_PUT32(updr->data.d_soa.minimum, cp);
		
		len = cp - start;
		break;		

	case ns_t_wks:
		start = cp;
		if (buflen < sizeof(uint32_t) + sizeof(uint8_t) + updr->data.d_wks.bitlen)
			return -1;
		memcpy(cp, &updr->data.d_wks.address, sizeof(updr->data.d_wks.address));
		cp += sizeof(updr->data.d_wks.address);
		*cp++ = updr->data.d_wks.protocol;
		memcpy(cp, updr->data.d_wks.bitmap, updr->data.d_wks.bitlen);
		cp += updr->data.d_wks.bitlen;
		len = cp - start;
		break;		
		
	case ns_t_rp:
		start = cp;
		n = ns_name_compress(updr->data.d_rp.mbox_dname,
				     cp, buflen,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;
		
		n = ns_name_compress(updr->data.d_rp.txt_dname,
				     cp, buflen,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;
		len = cp - start;
		break;

	case ns_t_x25:
		n = strlen(updr->data.d_x25);
		if (n > 255)
			return -1;
		len = n + 1;
		if (buflen < len)
			return -1;
		*cp++ = n;
		memcpy(cp, updr->data.d_x25, len);
		break;

	case ns_t_srv:
		if (buflen < 3*sizeof(uint16_t))
			return -1;
		start = cp;
		NS_PUT16(updr->data.d_srv.priority, cp);
		NS_PUT16(updr->data.d_srv.weight, cp);
		NS_PUT16(updr->data.d_srv.port, cp);
		buflen -= 3 * sizeof(uint16_t);
		n = ns_name_compress(updr->data.d_srv.target,
				     cp, buflen,
				     dnptrs, lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		len = cp - start;
		break;
	
	default:
		len = updr->data.d_any.rdlength;
		if (buflen < len)
			return -1;
		memcpy(cp, updr->data.d_any.rdata, updr->data.d_any.rdlength);
		break;
	}

	return len;
}

static inline char const *
lcase(char const *dname, char *buf)
{
	static char uc[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	size_t n = strcspn((char*)dname, uc);

	if (dname[n] == 0)
		return dname;
	else {
		char *p = buf;
		
		while (1) {
			memcpy(p, dname, n);
			if (dname[n]) {
				p[n] = tolower(dname[n]);
				n++;
			} else {
				p[n] = 0;
				break;
			}
			p += n;
			dname += n;
			n = strcspn(dname, uc);
		}

		return buf;
	}
}

int
nsu_nmkquery(res_state statp,
	     char const *zone,
	     nsu_rr *prereq, int num_prereq,
	     nsu_rr *update, int num_update,
	     struct nsu_tsig *tsig,
	     u_char *buf, int buflen)
{
	UPDATE_HEADER *hp;
	u_char *cp, *lencp;
	int i, n;
	u_char *dnptrs[20], **lastdnptr;
	int class = ns_c_in;
	int type = ns_t_soa;

	nsu_log(NSU_LOG_DEBUG, "%s: %s", __func__, zone);

	/*
	 * Initialize header fields.
	 */
	if (buf == NULL || buflen < HFIXEDSZ)
		return -1;
	memset(buf, 0, HFIXEDSZ);
	hp = (UPDATE_HEADER *) buf;

	/* We randomize the IDs every time.  The old code just
	   incremented by one after the initial randomization which
	   still predictable if the application does multiple
	   requests.  */
	int randombits;
	do {
#ifdef RANDOM_BITS
		RANDOM_BITS (randombits);
#else
		struct timeval tv;
		gettimeofday (&tv, NULL);
		randombits = (tv.tv_sec << 8) ^ tv.tv_usec;
#endif
	} while ((randombits & 0xffff) == 0);
	statp->id = (statp->id + randombits) & 0xffff;

	hp->id = statp->id;
	hp->opcode = ns_o_update;
	hp->rcode = NOERROR;
	hp->zocount = htons(1);
	hp->prcount = htons(num_prereq);
	hp->upcount = htons(num_update);
	
	cp = buf + HFIXEDSZ;
	buflen -= HFIXEDSZ;

	dnptrs[0] = buf;
	dnptrs[1] = NULL;
	lastdnptr = dnptrs + sizeof dnptrs / sizeof dnptrs[0];

	/* Zone section */
	n = ns_name_compress(zone, cp, buflen,
			     (const u_char **) dnptrs,
			     (const u_char **) lastdnptr);
	if (n < 0 || buflen < n + 4)
		return -1;
	cp += n;
	buflen -= n;
	NS_PUT16 (type, cp);
	NS_PUT16 (class, cp);

	/*
	 * Prerequisite Section
	 *
	 * 3.2.4 - Table Of Metavalues Used In Prerequisite Section
	 *
	 * CLASS    TYPE     RDATA    Meaning
	 * ------------------------------------------------------------
	 * ANY      ANY      empty    Name is in use
	 * ANY      rrset    empty    RRset exists (value independent)
	 * NONE     ANY      empty    Name is not in use
	 * NONE     rrset    empty    RRset does not exist
	 * zone     rrset    rr       RRset exists (value dependent)
	 */
	for (i = 0; i < num_prereq; i++) {
		int type;
		int class;

		switch (prereq[i].op) {
		case NSU_PREREQ_YXRRSET:
			/*
			 *  CLASS    TYPE     RDATA    Meaning
			 * -------------------------------------------------
			 *  ANY      rrset    empty    RRset exists
			 *                             (value independent)
			 */
			type = prereq[i].type;
			class = ns_c_any;
			break;

		case NSU_PREREQ_YXRRSETV:
			/*
			 *  CLASS    TYPE     RDATA    Meaning
			 * -------------------------------------------------
			 *  zone     rrset    rr       RRset exists
			 *                             (value dependent)
			 */
			type = prereq[i].type;
			class = ns_c_in;
			break;
			
		case NSU_PREREQ_NXRRSET:
			/*
			 *  CLASS    TYPE     RDATA    Meaning
			 * --------------------------------------------------
			 *  NONE     rrset    empty    RRset does not exist
			 */
			type = prereq[i].type;
			class = ns_c_none;
			break;

		case NSU_PREREQ_YXDOMAIN:
			/*
			 *  CLASS    TYPE     RDATA    Meaning
			 * ---------------------------------------------------
			 *  ANY      ANY      empty    Name is in use
			 */
			type = ns_t_any;
			class = ns_c_any;
			break;

		case NSU_PREREQ_NXDOMAIN:
			/*
			 *  CLASS    TYPE     RDATA    Meaning
			 * ---------------------------------------------------
			 *  NONE     rrset    empty    RRset does not exist
			 */
			type = prereq[i].type;
			class = ns_c_none;
			break;

		default:
			return -1;
		}

		n = ns_name_compress(prereq[i].name, cp, buflen,
				     (const u_char **) dnptrs,
				     (const u_char **) lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;

		if (buflen < 10)
			return -1;
		
		NS_PUT16(type, cp);
		NS_PUT16(class, cp);
		NS_PUT32(0, cp); //TTL
		lencp = cp;
		NS_PUT16(0, cp);

		buflen -= 10;
		
		if (prereq[i].op == NSU_PREREQ_YXRRSETV) {
			n = nsu_put_data(&prereq[i], cp, buflen,
					  (const u_char **) dnptrs,
					  (const u_char **) lastdnptr);
			if (n < 0)
				return -1;
			cp += n;
			buflen -= n;
			NS_PUT16(n, lencp);
		}
	}

	/*
	 * Update Section.
	 *
	 * 3.4.2.6 - Table Of Metavalues Used In Update Section
	 * 
	 * CLASS    TYPE     RDATA    Meaning
	 * ---------------------------------------------------------
	 * ANY      ANY      empty    Delete all RRsets from a name
	 * ANY      rrset    empty    Delete an RRset
	 * NONE     rrset    rr       Delete an RR from an RRset
	 * zone     rrset    rr       Add to an RRset
	 */
	for (i = 0; i < num_update; i++) {
		int type;
		int class;

		switch (update[i].op) {
		case NSU_UPDATE_DELALL:
			/* Delete all RRsets from a name */
			type = ns_t_any;
			class = ns_c_any;
			break;
			
		case NSU_UPDATE_DEL:
			/* Delete an RRset */
			type = update[i].type;
			class = ns_c_any;
			break;
				
		case NSU_UPDATE_DELV:
			/* Delete an RR from an RRset */
			type = update[i].type;
			class = ns_c_none;
			break;

		case NSU_UPDATE_ADD:
			/* Add to an RRset */
			type = update[i].type;
			class = ns_c_in;
			break;
			
		default:
			return -1;
		}


		n = ns_name_compress(update[i].name, cp, buflen,
				     (const u_char **) dnptrs,
				     (const u_char **) lastdnptr);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;

		if (buflen < 10)
			return -1;
		
		NS_PUT16(type, cp);
		NS_PUT16(class, cp);
		NS_PUT32(update[i].ttl, cp);
		lencp = cp;
		NS_PUT16(0, cp);

		buflen -= 10;
		
		switch (update[i].op) {
		case NSU_UPDATE_DELV:
		case NSU_UPDATE_ADD:
			n = nsu_put_data(&update[i], cp, buflen,
					 (const u_char **) dnptrs,
					 (const u_char **) lastdnptr);
			if (n < 0)
				return -1;
			cp += n;
			buflen -= n;
			NS_PUT16(n, lencp);
		}
	}

	if (tsig) {
		size_t msglen = cp - buf; /* Length of the original message */
		size_t tsig_digest_len = nsu_tsig_alg_digest_size(tsig->alg);
		struct timeval tv;
		char lcbuf[NS_MAXDNAME]; /* Buffer for converting domain names
		 		            to lower case. */
		
		u_char *tsig_name_ptr; /* Pointer to the start of the TSIG
					  name */
		size_t tsig_name_len;  /* Length of the TSIG name in wire
					  format */


		/*
		 * According to RFC 2845 (2.3. Record Format), RDATA
		 * has the following fields:
		 *
		 * N  Field Name      Data Type     Notes
		 * -----------------------------------------------------------
		 * 0  Algorithm Name  domain-name   Name of the algorithm
		 *                                  in domain name syntax.
		 * 1  Time Signed     u_int48_t     seconds since 1-Jan-70 UTC.
		 * 2  Fudge           u_int16_t     seconds of error permitted
                 *                                  in Time Signed.
		 * 3  MAC Size        u_int16_t     number of octets in MAC.
		 * 4  MAC             octet stream  defined by Algorithm Name.
		 * 5  Original ID     u_int16_t     original message ID
		 * 6  Error           u_int16_t     expanded RCODE covering
                 *                                  TSIG processing.
		 * 7  Other Len       u_int16_t     length, in octets, of
		 *                                  Other Data.
		 * 8  Other Data      octet stream  empty unless
		 *                                  Error == BADTIME
		 *
		 * NOTES:
		 *   1. As per RFC 2535 (8.1 Canonical RR Form), when
		 *      encoding the RR NAME and Algorithm Name field no name
		 *      compression is performed and all letters are converted
		 *      to lower case.
		 *
		 *   2. Fields 0-2 and 6-8 (inclusive) are digested into MAC.
		 *
		 */
		static int rdata_nlen = 16; /* Total length of RDATA minus
					       variable-length fields 0 and
					       4. */
		u_char *rdatap; /* Pointer to the start of rdata */
		size_t ilen;    /* Length of the initial portion of RDATA
				   (fields 0-2) */
		u_char *classp; /* Pointer to the CLASS field. Together with
				   TTL this makes 6 bytes of data. */
		u_char *macp;   /* Pointer to field 4 */
		u_char *tailp;  /* Pointer to field 6 */
		

		/* Create the RR */

                /* Save pointer to the start of the TSIG name. */
		tsig_name_ptr = cp;

		n = ns_name_compress(lcase(tsig->name, lcbuf),
				     cp, buflen,
				     NULL, NULL);
		if (n < 0)
			return -1;
		tsig_name_len = n;
		cp += n;
		buflen -= n;

		/* Need 10 bytes for RR fields */
		if (buflen < 10)
			return -1;

		NS_PUT16(ns_t_tsig, cp);

		/* Save the pointer to CLASS and TTL */
		classp = cp;
		NS_PUT16(ns_c_any, cp);
		NS_PUT32(0, cp);

		/* Save pointer to the RDLENGTH slot */
		lencp = cp;
		NS_PUT16(0, cp);

		buflen -= 10;
		
		/* Mark start of the RDATA */
		rdatap = cp;

                /* RDATA
		 * Field Name       Data Type      Notes
		 * -----------------------------------------------------------
		 * Algorithm Name   domain-name    Name of the algorithm
                 *                                 in domain name syntax.
		 */

		n = ns_name_compress(lcase(nsu_tsig_alg_name(tsig->alg),
					   lcbuf),
				     cp, buflen,
				     NULL, NULL);
		if (n < 0)
			return -1;
		cp += n;
		buflen -= n;

		if (buflen < rdata_nlen + tsig_digest_len)
			return -1;

		/*
		 * -----------------------------------------------------------
		 * Time Signed      u_int48_t      seconds since 1-Jan-70 UTC.
		 *
		 * FIXME: Pack u_int48_t as two zero bytes followed by
		 * uint32_t value in network order.
		 */		
		gettimeofday(&tv, NULL);
		NS_PUT16(0, cp);
		NS_PUT32(tv.tv_sec, cp);

		/*
		 * -----------------------------------------------------------
		 * Fudge            u_int16_t      seconds of error permitted
		 *                                 in Time Signed.
		 */
		NS_PUT16(tsig->fudge, cp);

		/* Save the length of the initial data portion */
		ilen = cp - rdatap;

		/* Add the digest length */
		NS_PUT16(tsig_digest_len, cp);

                /* Save pointer to the MAC area */
		macp = cp;

                /* Reserve sig_len bytes for MAC */
		cp += tsig_digest_len;
		buflen -= tsig_digest_len;

		/* Original ID */
		*cp++ = buf[0];
		*cp++ = buf[1];

                /* Save pointer to the tail area */
		tailp = cp;

                /* Error */
		NS_PUT16(hp->rcode, cp);
		/* Other len */
		NS_PUT16(0, cp);

		/* Hash the original message */
		nsu_tsig_update(tsig, buf, msglen);

		/* Add NAME to the digest */
		nsu_tsig_update(tsig, tsig_name_ptr, tsig_name_len);

		/* Add CLASS and TTL to the digest. */
		nsu_tsig_update(tsig, classp, 6);

		/* Hash the Algorighm Name, Time Signed and Fudge */
		nsu_tsig_update(tsig, rdatap, ilen);

                /* Hash the tail portion of rdata: Error, Other Length (0) */
		nsu_tsig_update(tsig, tailp, cp-tailp);

                /* Add the digest */
		nsu_tsig_digest(tsig, tsig->digest, tsig_digest_len);
		memcpy(macp, tsig->digest, tsig_digest_len);

		/* Store the actual RDATA length */
		NS_PUT16(cp - rdatap, lencp);

		hp->adcount = htons(1);
	}

	return cp - buf;
}

int
nsu_mkquery(char const *zone,
	    nsu_rr *prereq, int num_prereq,
	    nsu_rr *update, int num_update,
	    struct nsu_tsig *tsig,
	    u_char *buf, int buflen)
{
	return nsu_nmkquery(&_res,
			    zone, prereq, num_prereq,
			    update, num_update,
			    tsig,
			    buf, buflen);
}
