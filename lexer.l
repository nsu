%{
#include <stdio.h>
#include <unistd.h>	
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ctype.h>
#include <limits.h>
#include "nsu.h"
#include "nsu_app.h"
#include "y.tab.h"
	
#define MAX_STRING 1024
static char strbuf[MAX_STRING];
static size_t stridx;

static void
str_add(char const *str, size_t len)
{
        if (len + 1 > MAX_STRING - stridx)
		abend("input string too long");
        memcpy(strbuf + stridx, str, len);
        stridx += len;
}

static void
str_start(char const *str, size_t len)
{
        stridx = 0;
        str_add(str, len);
}

static void
str_esc(int c)
{
	if (stridx == MAX_STRING-1)
		abend("input string too long");
	strbuf[stridx++] = c;
}

static char *
str_stop(void)
{
	char *p = alloc_string(stridx);
	memcpy(p, strbuf, stridx);
	p[stridx] = 0;
	return p;
}
        
static char *
str_new(char const *str, size_t len)
{
        str_start(str, len);
        return str_stop();
}

struct strlist {
	struct strlist *prev;
	char str[1];
};

static struct strlist *strtail;

char *
alloc_string(size_t len)
{
	struct strlist *sp = malloc(sizeof(*sp) +  len);
	if (!sp)
		abend_nomem();
	sp->prev = strtail;
	strtail = sp;
	return sp->str;
}

void
drop_last_string(void)
{
	struct strlist *prev = strtail->prev;
	free(strtail);
	strtail = prev;
}

void
dealloc_strings(void)
{
	while (strtail)
		drop_last_string();
}

int
xdig(int c)
{
	static char hexchr[] = "0123456789ABCDEFabcdef";
	int n;
	char *p = strchr(hexchr, c);
	if (!p)
		return -1;
	n = p - hexchr;
	if (n > 15)
		n -= 6;
	return n;
}

/*
 * On entry, yytext contains rdata in format:
 *
 *     #\ LEN HEXSTRING
 *
 * Parse the data and place the result in yylval.r.data.d_any.
 *
 * Return 0 on success and -1 on error.
 */
static int
typex_parse(void)
{
	size_t n, i;
	u_char *val;
	char *p;

	p = yytext + 2;
	while (isspace(*p)) p++;
	if ((n = strtoul(p, &p, 10)) < ULONG_MAX && n < ((size_t)-1) / 2) {
		size_t len;
		while (isspace(*p)) p++;
		len = yyleng - (p - yytext);
		if (len == n * 2) {
			val = alloc_string(n);
			for (i = 0; i < n; i++) {
				int x = xdig(*p);
				if (x == -1)
					goto err;
				val[i] = x << 4;
				p++;
				x = xdig(*p);
				if (x == -1)
					goto err;
				val[i] |= x;
				p++;
			}
			yylval.r.data.d_any.rdlength = n;
			yylval.r.data.d_any.rdata = val;
			return 0;
		err:
			drop_last_string();
		}
	}
	return -1;
}

static nsu_point point;

static inline void
advance_line(void)
{
	++point.line;
	point.col = 0;
}


static int inargc;
static char **inargv;
static int next;
static char *curp;
static int state;

static inline int
is_interactive(void)
{
	return !inargv && isatty(fileno(yyin)) && isatty(1);
}

static int need_prompt = 1;

static inline void
prompt(void)
{
	if (need_prompt) {
		printf("nsu> ");
		fflush(stdout);
	}
}

#define ST_DFL 0
#define ST_QUOTE 0x1
#define ST_DELIM 0x2
#define ST_STOP 0x4

static int
fillbuf(char *buf, size_t max_size)
{
	if (inargv) {
		size_t total = 0;
		char const specials[] = " \"\t\n;";
		char const escapable[] = "\\\"";
  
		while (total < max_size) {
			size_t len;

			if (curp == NULL || *curp == 0) {
				if (state & ST_QUOTE) {
					buf[total++] = '"';
					state &= ~ST_QUOTE;
					continue;
				}
				if (state & ST_STOP)
					break;
				if (next == inargc) {
					buf[total++] = ';';
					state |= ST_STOP;
					break;
				}
				if (next)
					state |= ST_DELIM;
				curp = inargv[next++];
			}
	  
			if (state & ST_DELIM) {
				buf[total++] = ' ';
				state &= ~ST_DELIM;
				continue;
			}

			len = strlen(curp);
			if (!(state & ST_QUOTE)) {
				size_t n = strcspn(curp, specials);
				if (curp[n]) {
					if (n == 0) {
						buf[total++] = *curp++;
						continue;
					} else if (curp[n + 1] == 0) {
						if (n > max_size)
							n = max_size;
						memcpy(buf + total, curp, n);
						total += n;
						curp += n;
						continue;
					} else {
						buf[total++] = '"';
						state |= ST_QUOTE;
						continue;
					}
				} else
					state &= ~ST_QUOTE;
			}
			
			if ((state & ST_QUOTE) && strchr(escapable, *curp)) {
				if (total + 2 > max_size)
					break;
				buf[total++] = '\\';
				curp++;
			}
			buf[total++] = *curp++;
		}
		return total;
	} else if (is_interactive()) {
		int n;
		
		prompt();
		if (fgets(buf, max_size, yyin) == NULL)
			return 0;
		n = strlen(buf);
		need_prompt = (n > 0 && buf[n-1] == '\n');
		return n;
	}
	return fread(buf, 1, max_size, yyin);
}

void
lexer_from_file(char const *name)
{
	if (strcmp(name, "-") == 0) {
		yyin = stdin;
		point.file = "<stdin>";
	} else {
		yyin = fopen(name, "r");
		if (!yyin)
			abend("can't open input file %s: %s",
			      name, strerror(errno));
		point.file = name;
	}
	point.line = 1;
	point.col = 1;
}

void
lexer_from_argv(int argc, char **argv)
{
        inargc = argc;
        inargv = argv;
        next = 0;
        curp = NULL;

	point.file = "<argv>";
	point.line = 1;
	point.col = 1;
}

#define YY_USER_ACTION					    \
	do {						    \
		if (YYSTATE == 0) {			    \
			yylloc.beg = point;		    \
			yylloc.beg.col++;		    \
		}					    \
		point.col += yyleng;			    \
		yylloc.end = point;			    \
	} while (0);

#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) result = fillbuf (buf, max_size)

%}
%x STR DOM TYPEX
D [0-9]
H [0-9a-fA-F]
WS [ \t]
C [^ \t\"\n;]
%%
nxdomain        return T_NXDOMAIN;
yxdomain        return T_YXDOMAIN;
nxrrset         return T_NXRRSET;
yxrrset         return T_YXRRSET;
del|delete      return T_DELETE;
add             return T_ADD;
update          return T_UPDATE;
send            return T_SEND;

\"[^\\"]*\"     { yylval.s = str_new(yytext + 1, yyleng - 2);
                  return T_STRING; }
\"[^\\"]*\\.    { BEGIN(STR);
                  str_start(yytext + 1, yyleng - 3);
                  str_esc(yytext[yyleng-1]); }
<STR>[^\\"]*\\. { str_add(yytext, yyleng - 2);
                  str_esc(yytext[yyleng-1]); }
<STR>[^\\"]*\"  { str_add(yytext, yyleng - 1);
                  yylval.s = str_stop();
                  BEGIN(INITIAL);
                  return T_STRING; }

<DOM>{C}+       { yylval.s = str_new(yytext, yyleng);
                  BEGIN(INITIAL);
                  return T_STRING; }

<TYPEX>{WS}+    ;
<TYPEX>#\\{WS}+{D}+{WS}+{H}+ {
                  BEGIN(INITIAL);
                  if (typex_parse())
			  yyless(0);
                  else
 			  return T_TYPEX;
}

<DOM,TYPEX>.    { yyless(0); BEGIN(INITIAL); }

{D}+            { errno = 0;
                  yylval.n = strtoul(yytext, NULL, 10);
                  if (errno) {
                       abend("%s: %s", yytext, strerror(errno));
                  }
                  return T_NUMBER; }

"("|")"         return yytext[0];

{C}+            { yylval.s = str_new(yytext, yyleng);
                  return T_STRING; }

\n              { advance_line(); return T_EOL; }
";"             return T_EOL;

{WS}+           { if (inargv) {
		      advance_line();
		      yylloc.beg = point;
		      yylloc.beg.col++;
	          } }

\\.             { str_start(NULL, 0);
                  str_esc(yytext[1]);
                  yylval.s = str_stop();
                  return T_STRING; }

.               { abend("stray character %#o in input stream", yytext[0]); }
%%
int
yywrap(void)
{
    return 1;
}

void
need_domain(void)
{
    BEGIN(DOM);
}

static char const *
token_type_str(int tok, char *cs)
{
	switch (tok) {
	case T_NXDOMAIN:
		return "nxdomain";
	case T_YXDOMAIN:
		return "yxdomain";
	case T_NXRRSET:
		return "nxrrset";
	case T_YXRRSET:
		return "yxrrset";
	case T_ADD:
		return "add";
	case T_DELETE:
		return "delete";
	case T_SEND:
		return "send";
	case T_UPDATE:
		return "update";
	case T_STRING:
		return "string";
	case T_NUMBER:
		return "number";
	case T_TYPEX:
		return "RDATA";
	}
	if (tok < 128 && cs) {
		cs[0] = tok;
		cs[1] = 0;
		return cs;
	}
	return NULL;
};

enum {
	RDATA_PARSE_OK,
	RDATA_PARSE_NONE,
	RDATA_PARSE_ERR
};

static void
unexpected_token_error(char const *exp)
{
	abend("expected %s, but found %s", exp, yytext);
}

enum {
	EOF_ERR,
	EOF_OK,
};

static int
gettoken(int expect_type, int eof, char const *expect_str)
{
	int rc = yylex();
        char cs[2];
	char const *tstr;
	
        if (eof == EOF_OK) {
                switch (rc) {
	        case T_EOL:
		        yyless(0);
		        return RDATA_PARSE_NONE;

	        case 0:
		        return RDATA_PARSE_NONE;
                }
        }

        if (expect_type == T_STRING) {
                switch (rc) {
                case T_STRING:
                case T_NUMBER:
                case T_TYPEX:
                        break;
                default:
                        if ((tstr = token_type_str(rc, NULL)) != NULL) {
                                yylval.s = str_new(tstr, strlen(tstr));
                                rc = T_STRING;
                        }
                }
        }

	if (rc != expect_type) {
                if (expect_str == NULL) {
			expect_str = token_type_str(expect_type, cs);
                }
                unexpected_token_error(expect_str);
		return RDATA_PARSE_ERR;
	}
	return RDATA_PARSE_OK;
}

static int
gettoken_domain(int eof, char const *expect_str, char **ret_val)
{
	int rc;
	need_domain();
	rc = gettoken(T_STRING, eof, expect_str);
	if (rc == RDATA_PARSE_OK && ret_val)
		*ret_val = yylval.s;
	return rc;
}

static int
gettoken_number(int eof, char const *expect_str, int width, void *data)
{
	size_t limit;
	int rc;
	
	rc = gettoken(T_NUMBER, eof, expect_str);
	if (rc != RDATA_PARSE_OK)
		return rc;
	switch (width) {
	case sizeof(uint8_t):
		limit = (uint8_t)-1;
		break;
		
	case sizeof(uint16_t):
		limit = (uint16_t)-1;
		break;
		
	case sizeof(uint32_t):
		limit = (uint32_t)-1;
		break;
		
	default:
		abort();
	}
	
	if (yylval.n > limit) {
		panic("%s out of allowed range [0,%zu]",
		      expect_str ? expect_str : token_type_str(T_NUMBER, NULL),
		      limit);
		return RDATA_PARSE_ERR;
	}
	switch (width) {
	case sizeof(uint8_t):
		*(uint8_t*)data = yylval.n;
		break;

	case sizeof(uint16_t):
		*(uint16_t*)data = yylval.n;
		break;

	case sizeof(uint32_t):
		*(uint32_t*)data = yylval.n;
		break;

	default:
		abort();
	}
	return RDATA_PARSE_OK;
}

static int
gettoken_v(int *expect_types, int num_types, int eof, char const *expect_str,
	   int *ret_tok)
{
	int rc = yylex();
        char cs[2];
	char const *tstr;
	int i;
	
        if (eof == EOF_OK) {
                switch (rc) {
	        case T_EOL:
		        yyless(0);
		        return RDATA_PARSE_NONE;

	        case 0:
		        return RDATA_PARSE_NONE;
                }
        }

	for (i = 0; i < num_types; i++) {
		if (expect_types[i] == T_STRING) {
			switch (rc) {
			case T_STRING:
			case T_NUMBER:
			case T_TYPEX:
				break;
			default:
				if ((tstr = token_type_str(rc, NULL)) != NULL) {
					yylval.s = str_new(tstr, strlen(tstr));
					rc = T_STRING;
				}
			}
		}

		if (rc == expect_types[i]) {
			*ret_tok = rc;
			return RDATA_PARSE_OK;
		}
	}

	if (expect_str == NULL) {
		expect_str = token_type_str(expect_types[0], cs);
	}
	unexpected_token_error(expect_str);
	return RDATA_PARSE_ERR;
}

static int
a_parser(nsu_rr *rr)
{
	struct addrinfo hints, *res;

        int rc = gettoken(T_STRING, EOF_OK, "IPv4 address");
	if (rc != RDATA_PARSE_OK)
		return rc;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	
	if (getaddrinfo(yylval.s, NULL, &hints, &res)
	    || res->ai_addrlen != sizeof(struct sockaddr_in)) {
		abend("%s: invalid IP address", yylval.s);
		return RDATA_PARSE_ERR;
	}
	
	memcpy(&rr->data.d_a,
	       &((struct sockaddr_in*)res->ai_addr)->sin_addr,
	       sizeof(rr->data.d_a));
	freeaddrinfo(res);
	return RDATA_PARSE_OK;
}

static int
txt_parser(nsu_rr *rr)
{
        int rc = gettoken(T_STRING, EOF_OK, NULL);
	if (rc != RDATA_PARSE_OK)
		return rc;
	rr->data.d_txt = yylval.s;
	return RDATA_PARSE_OK;
}

static int
domain_name_parser(nsu_rr *rr)
{
	return gettoken_domain(EOF_OK, "domain-name", &rr->data.d_name);
}

static int
mx_parser(nsu_rr *rr)
{
	int rc;

	rc = gettoken_number(EOF_OK, "preference",
			     sizeof(rr->data.d_mx.pref),
			     &rr->data.d_mx.pref);
	if (rc != RDATA_PARSE_OK)
		return rc;	
	return gettoken_domain(EOF_ERR, "MX name", &rr->data.d_mx.name);
}

static int
hinfo_parser(nsu_rr *rr)
{
        int rc = gettoken(T_STRING, EOF_OK, "CPU string");
	if (rc != RDATA_PARSE_OK)
		return rc;
	rr->data.d_hinfo.cpu = yylval.s;
        rc = gettoken(T_STRING, EOF_ERR, "OS string");
	if (rc != RDATA_PARSE_OK)
		return rc;
	rr->data.d_hinfo.os = yylval.s;
	return RDATA_PARSE_OK;
}
	    
static int
minfo_parser(nsu_rr *rr)
{
        int rc;

	rc = gettoken_domain(EOF_OK, "RESP-MAILBOX",
			     &rr->data.d_minfo.rmailbx);
	if (rc != RDATA_PARSE_OK)
		return rc;
	return gettoken_domain(EOF_ERR, "ERROR-MAILBOX",
			       &rr->data.d_minfo.emailbx);
}

static int
soa_parser(nsu_rr *rr)
{
        int rc;

	rc = gettoken_domain(EOF_OK, "primary NS server",
			     &rr->data.d_soa.mname);
	if (rc != RDATA_PARSE_OK)
		return rc;
	
	rc = gettoken_domain(EOF_ERR, "responsible person mailbox",
			     &rr->data.d_soa.rname);
	if (rc != RDATA_PARSE_OK)
		return rc;

	rc = gettoken_number(EOF_ERR, "serial",
			     sizeof(rr->data.d_soa.serial),
			     &rr->data.d_soa.serial);
	if (rc != RDATA_PARSE_OK)
		return rc;

	rc = gettoken_number(EOF_ERR, "refresh",
			     sizeof(rr->data.d_soa.refresh),
			     &rr->data.d_soa.refresh);
	if (rc != RDATA_PARSE_OK)
		return rc;

	rc = gettoken_number(EOF_ERR, "retry", 
			     sizeof(rr->data.d_soa.retry),
			     &rr->data.d_soa.retry);
	if (rc != RDATA_PARSE_OK)
		return rc;
	
	rc = gettoken_number(EOF_ERR, "expire",
			     sizeof(rr->data.d_soa.expire),
			     &rr->data.d_soa.expire);
	if (rc != RDATA_PARSE_OK)
		return rc;
	
	return gettoken_number(EOF_ERR, "minimum",
			       sizeof(rr->data.d_soa.minimum),
			       &rr->data.d_soa.minimum);
}

#define BITMAP_SIZE (USHRT_MAX/8)
typedef	uint8_t BITMAP[BITMAP_SIZE];

#define BITMAP_INIT(b) memset(b, 0, BITMAP_SIZE)
#define __BITMAP_BYTE(n) ((n)/8)
#define __BITMAP_BIT(n) (1<<(7-(n)%8))
#define BITMAP_SET(b,n) ((b)[__BITMAP_BYTE(n)] |= __BITMAP_BIT(n))
#define BITMAP_ISSET(b,n) ((b)[__BITMAP_BYTE(n)] & __BITMAP_BIT(n))

static int
wks_parser(nsu_rr *rr)
{
	struct addrinfo hints, *res;
	BITMAP bitmap;
	int port_max = 0;
	int tok;
	static int types[] = {
		T_STRING,
		T_NUMBER,
		')'
	};
	struct protoent *proto = NULL;
	
        int rc = gettoken(T_STRING, EOF_OK, "IPv4 address");
	if (rc != RDATA_PARSE_OK)
		return rc;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	
	if (getaddrinfo(yylval.s, NULL, &hints, &res)
	    || res->ai_addrlen != sizeof(struct sockaddr_in)) {
		panic("%s: invalid IP address", yylval.s);
		return RDATA_PARSE_ERR;
	}
	
	memcpy(&rr->data.d_wks.address,
	       &((struct sockaddr_in*)res->ai_addr)->sin_addr,
	       sizeof(rr->data.d_a));
	freeaddrinfo(res);

        rc = gettoken_v(types, 2, EOF_ERR, "protocol number", &tok);
	if (rc != RDATA_PARSE_OK)
		return rc;
	if (tok == T_STRING) {
		proto = getprotobyname(yylval.s);
		if (!proto) {
			yyerror("unknown protocol name");
			return RDATA_PARSE_ERR;
		}
		rr->data.d_wks.protocol = proto->p_proto;
	} else { /* T_NUMBER */
		if (yylval.n > 255) {
			yyerror("protocol number out of range");
			return RDATA_PARSE_ERR;
		}
		proto = getprotobynumber(yylval.n);
		if (!proto) {
			yyerror("unknown protocol name");
			return RDATA_PARSE_ERR;
		}
		rr->data.d_wks.protocol = yylval.n;
	}
	
        rc = gettoken('(', EOF_ERR, NULL);
	if (rc != RDATA_PARSE_OK)
		return rc;
	BITMAP_INIT(bitmap);
	while (1) {
		int port;
		
		rc = gettoken_v(types, sizeof(types)/sizeof(types[0]),
				EOF_ERR, "service", &tok);
		if (rc != RDATA_PARSE_OK)
			return rc;
		if (tok == ')')
			break;
		if (tok == T_STRING) {
			struct servent *sent = getservbyname(yylval.s, NULL);
			if (!sent) {
				panic("%s: unknown service", yylval.s);
				return RDATA_PARSE_ERR;
			}
			port = ntohs(sent->s_port);
		} else {
			port = yylval.n;
		}
		BITMAP_SET(bitmap, port);
		if (port > port_max)
			port_max = port;
	}

	rr->data.d_wks.bitlen = __BITMAP_BYTE(port_max) + 1;
	rr->data.d_wks.bitmap = alloc_string(rr->data.d_wks.bitlen);
	memcpy(rr->data.d_wks.bitmap, bitmap, rr->data.d_wks.bitlen);
	
	return RDATA_PARSE_OK;
}

static int
rp_parser(nsu_rr *rr)
{
        int rc;

	rc = gettoken_domain(EOF_OK, "MBOX-DNAME", &rr->data.d_rp.mbox_dname);
	if (rc != RDATA_PARSE_OK)
		return rc;

        return gettoken_domain(EOF_ERR, "TXT-DNAME", &rr->data.d_rp.txt_dname);
}

static int
x25_parser(nsu_rr *rr)
{
        int rc = gettoken(T_STRING, EOF_OK, NULL);
	if (rc != RDATA_PARSE_OK)
		return rc;
	rr->data.d_x25 = yylval.s;
	return RDATA_PARSE_OK;
}

static int
srv_parser(nsu_rr *rr)
{
	int rc;

	rc = gettoken_number(EOF_OK, "priority",
			     sizeof(rr->data.d_srv.priority),
			     &rr->data.d_srv.priority);
	if (rc != RDATA_PARSE_OK)
		return rc;

	rc = gettoken_number(EOF_ERR, "weight",
			     sizeof(rr->data.d_srv.weight),
			     &rr->data.d_srv.weight);
	if (rc != RDATA_PARSE_OK)
		return rc;
	
	rc = gettoken_number(EOF_ERR, "port",
			     sizeof(rr->data.d_srv.port),
			     &rr->data.d_srv.port);
	if (rc != RDATA_PARSE_OK)
		return rc;
	return gettoken_domain(EOF_ERR, "target", &rr->data.d_srv.target);
}

static int
def_type_parser(nsu_rr *rr)
{
	int rc;
	BEGIN(TYPEX);
	rc = gettoken(T_TYPEX, EOF_OK, NULL);
	if (rc != RDATA_PARSE_OK)
		return rc;
	rr->data = yylval.r.data;
	return RDATA_PARSE_OK;
}

typedef int (*RDATA_PARSER)(nsu_rr *rr);

static RDATA_PARSER rdata_parser[] = {
	[ns_t_a] = a_parser,
	[ns_t_txt] = txt_parser,
	[ns_t_ns] = domain_name_parser,
	[ns_t_cname] = domain_name_parser,
	[ns_t_ptr] = domain_name_parser,
	[ns_t_mx] = mx_parser,
	[ns_t_hinfo] = hinfo_parser,
	[ns_t_mb] = domain_name_parser,
	[ns_t_md] = domain_name_parser,
	[ns_t_mf] = domain_name_parser,
	[ns_t_mg] = domain_name_parser,
	[ns_t_minfo] = minfo_parser,
	[ns_t_mr] = domain_name_parser,
	[ns_t_null] = def_type_parser,
	[ns_t_soa] = soa_parser,
	[ns_t_wks] = wks_parser,
	[ns_t_dname] = domain_name_parser,
	[ns_t_rp] = rp_parser,
	[ns_t_x25] = x25_parser,
	[ns_t_srv] = srv_parser
};
static size_t num_rdata = sizeof(rdata_parser) / sizeof(rdata_parser[0]);

void
rdata_list(void)
{
	int i;

	for (i = 0; i < num_rdata; i++) {
		if (rdata_parser[i])
			printf("%s\n", nsu_type_to_str(i));
	}
}

int
rdata_parse(nsu_rr *rr, int type)
{
	RDATA_PARSER parser;
	int rc;
	
	memset(rr, 0, sizeof(*rr));
	rr->type = ns_t_any;
	if (type >= 0 && type < num_rdata && rdata_parser[type]) {
		parser = rdata_parser[type];
	} else
		parser = def_type_parser;
	
	rc = parser(rr);
	if (rc == RDATA_PARSE_OK)
		rr->type = type;
	else if (rc == RDATA_PARSE_ERR)
		return -1;
	return 0;
}
	    
void
error_at_locus(nsu_locus *locus, const char *fmt, ...)
{
	va_list ap;
	if (!locus)
		locus = &yylloc;
	YY_LOCATION_PRINT(stderr, *locus);
	fwrite(": ", 2, 1, stderr);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
}
	       

