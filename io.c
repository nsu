#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "nsu.h"

static int
nsu_nsend_dg(res_state statp, nsu_sockaddr *a, u_char *qbuf, size_t qlen,
	     u_char *rbuf, size_t rlen, size_t *return_len)
{
	struct pollfd pfd;
	UPDATE_HEADER *uhp;
	int retry = 0;
	int result = NSU_SEND_SYSERR;
	int ec;

	nsu_log(NSU_LOG_DEBUG, "sending query %d via UDP",
		((HEADER*)qbuf)->id);
	pfd.fd = socket(a->addr.sa.sa_family, SOCK_DGRAM, 0);
	if (pfd.fd == -1) {
		return NSU_SEND_SYSERR;
	}
	pfd.events = POLLOUT;
	while (1) {
		ssize_t n;
		int rc;
		
		rc = poll(&pfd, 1, statp->retrans * 1000);
		if (rc < 0) {
			result = NSU_SEND_SYSERR;
			break;
		}
		if (rc == 0) {
			if (retry == statp->retry) {
				result = NSU_SEND_TIMEOUT;
				break;
			}
			retry++;
			continue;
		}
		if (pfd.revents & POLLOUT) {
			n = sendto(pfd.fd, qbuf, qlen, MSG_NOSIGNAL,
				   &a->addr.sa, a->len);
			if (n < 0) {
				if (errno == EINTR || errno == EAGAIN) {
					continue;
				}
				result = NSU_SEND_SYSERR;
				break;
			}
			pfd.events = POLLIN;
		} else if (pfd.revents & POLLIN) {
			nsu_sockaddr fromaddr;
			HEADER *hp;

			fromaddr.len = sizeof(fromaddr.addr);
			n = recvfrom(pfd.fd, rbuf, rlen, 0,
				     &fromaddr.addr.sa, &fromaddr.len);
			if (n < 0) {
				if (errno == EINTR || errno == EAGAIN) {
					continue;
				}
				result = NSU_SEND_SYSERR;
				break;
			}
			pfd.events = 0;
			if (!(statp->options & RES_INSECURE1) &&
			    !(fromaddr.len == a->len &&
			      memcmp(&fromaddr.addr, &a->addr, a->len) == 0)) {
				nsu_log(NSU_LOG_DEBUG, "%s",
					"response from the wrong server");
				//FIXME
				result = NSU_SEND_BADSERV;
				break;
			}

			hp = (HEADER*)rbuf;
			if (hp->tc) {
				result = NSU_SEND_RETRYTCP;
				break;
			}
			*return_len = n;
			result = NSU_SEND_OK;
			break;
		} else if (pfd.revents & POLLERR) {
			result = NSU_SEND_SYSERR;
			break;
		} else if (pfd.revents & POLLHUP) {
			result = NSU_SEND_PEERCLOSE;
			break;
		} else if (pfd.revents & POLLNVAL) {
			result = NSU_SEND_SYSERR;
			errno = EINVAL;
			break;
		} else
			abort();
	}

	if (result == NSU_SEND_SYSERR)
		ec = errno;
	close(pfd.fd);
	if (result == NSU_SEND_SYSERR)
		errno = ec;
	
	return result;
}

static int
nsu_nsend_vc(res_state statp, nsu_sockaddr *a, u_char *qbuf, size_t qlen,
	     u_char *rbuf, size_t rlen, size_t *return_len)
{
	struct pollfd pfd;
	int retry = 0;
	int result = NSU_SEND_SYSERR;
	int ec;
	
	nsu_log(NSU_LOG_DEBUG, "sending query %d via TCP",
		((HEADER*)qbuf)->id);
	pfd.fd = socket(a->addr.sa.sa_family, SOCK_STREAM, 0);
	if (pfd.fd == -1) {
		return NSU_SEND_SYSERR;
	}
	pfd.events = POLLOUT;

	if (connect(pfd.fd, &a->addr.sa, a->len)) {
		ec = errno;
		close(pfd.fd);
		errno = ec;
		return NSU_SEND_SYSERR;
	}

	//FIXME: bind?
	
	while (1) {
		ssize_t n;
		int rc;
		
		rc = poll(&pfd, 1, statp->retrans * 1000);
		if (rc < 0) {
			result = NSU_SEND_SYSERR;
			break;
		}
		if (rc == 0) {
			if (retry == statp->retry) {
				result = NSU_SEND_TIMEOUT;
				break;
			}
			retry++;
			continue;
		}
		if (pfd.revents & POLLOUT) {
			n = write(pfd.fd, qbuf, qlen);
			if (n < 0) {
				if (errno == EINTR || errno == EAGAIN) {
					continue;
				}
				result = NSU_SEND_SYSERR;
				break;
			}
			pfd.events = POLLIN;
		} else if (pfd.revents & POLLIN) {
			n = read(pfd.fd, rbuf, rlen);
			if (n < 0) {
				if (errno == EINTR || errno == EAGAIN) {
					continue;
				}
				result = NSU_SEND_SYSERR;
				break;
			}
			*return_len = n;
			result = NSU_SEND_OK;
			break;
		} else if (pfd.revents & POLLERR) {
			result = NSU_SEND_SYSERR;
			break;
		} else if (pfd.revents & POLLHUP) {
			result = NSU_SEND_PEERCLOSE;
			break;
		} else if (pfd.revents & POLLNVAL) {
			result = NSU_SEND_SYSERR;
			errno = EINVAL;
			break;
		} else
			abort();
	}

	if (result == NSU_SEND_SYSERR)
		ec = errno;
	close(pfd.fd);
	if (result == NSU_SEND_SYSERR)
		errno = ec;
	
	return result;
}

int
nsu_nsend(res_state statp, nsu_sockaddr *a, u_char *qbuf, size_t qlen,
	  u_char *rbuf, size_t rlen, size_t *return_len)
{
	int result;
	
	if (!qbuf || !rbuf || qlen < sizeof(UPDATE_HEADER) || rlen < sizeof(UPDATE_HEADER)) {
		errno = EINVAL;
		return NSU_SEND_SYSERR;
	}

	result = nsu_nsend_dg(statp, a, qbuf, qlen, rbuf, rlen, return_len);
	if (result == NSU_SEND_RETRYTCP && (statp->options & RES_USEVC))
		result = nsu_nsend_vc(statp, a, qbuf, qlen, rbuf, rlen, return_len);

	return result;
}

	
