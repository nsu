void need_domain();

extern unsigned long default_ttl;
extern int yydebug;
extern int yy_flex_debug;
extern struct nsu_tsig *tsig;

void panic(char const *fmt, ...);
void abend(char const *fmt, ...);
void abend_nomem(void);
int yyerror(char const *);
int yyparse(void);
int yylex(void);
	
void lexer_from_file(char const *name);
void lexer_from_argv(int argc, char **argv);

int run_update(nsu_rr *prereq, int num_prereq,
	       nsu_rr *update, int num_update);

char *alloc_string(size_t len);
void dealloc_strings(void);

void rdata_list(void);
int rdata_parse(nsu_rr *rr, int type);

/* Position in input file */
typedef struct point {
	char *file;       /* file name; <stdin> for tty, and <argv> for command
			     line. */
	unsigned line;    /* When reading input from file: line number;
			     When reading it from command line arguments: index
			     of the argument in argv. */  
	unsigned col;     /* column number */ 
} nsu_point;

/* Location in input file */
typedef struct locus {
	nsu_point beg, end;
} nsu_locus;

#define YYLTYPE nsu_locus

#define YYLLOC_DEFAULT(Current, Rhs, N)			      \
	do {						      \
		if (N) {				      \
			(Current).beg = YYRHSLOC(Rhs, 1).beg; \
			(Current).end = YYRHSLOC(Rhs, N).end; \
		} else {				      \
			(Current).beg = YYRHSLOC(Rhs, 0).end; \
			(Current).end = (Current).beg;	      \
		}					      \
	} while (0)

#define YY_LOCATION_PRINT(File, Loc)					\
	do {								\
		if ((Loc).beg.col == 0)					\
			fprintf (File, "%s:%u",				\
				 (Loc).beg.file,			\
				 (Loc).beg.line);			\
		else if (strcmp ((Loc).beg.file, (Loc).end.file))	\
			fprintf (File, "%s:%u.%u-%s:%u.%u",		\
				 (Loc).beg.file,			\
				 (Loc).beg.line, (Loc).beg.col,		\
				 (Loc).end.file,			\
				 (Loc).end.line, (Loc).end.col);	\
		else if ((Loc).beg.line != (Loc).end.line)		\
			fprintf (File, "%s:%u.%u-%u.%u",		\
				 (Loc).beg.file,			\
				 (Loc).beg.line, (Loc).beg.col,		\
				 (Loc).end.line, (Loc).end.col);	\
		else if ((Loc).beg.col != (Loc).end.col)		\
			fprintf (File, "%s:%u.%u-%u",			\
				 (Loc).beg.file,			\
				 (Loc).beg.line, (Loc).beg.col,		\
				 (Loc).end.col);			\
		else							\
			fprintf (File, "%s:%u.%u",			\
				 (Loc).beg.file,			\
				 (Loc).beg.line,			\
				 (Loc).beg.col);			\
	} while (0)

void error_at_locus(nsu_locus *locus, const char *fmt, ...);

