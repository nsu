CFLAGS=-ggdb -Wall

APPSRC=main.c y.tab.c lex.yy.c
APPOBJ=${APPSRC:.c=.o}

NSUSRC=nsu.c typestr.c alg.c log.c io.c
NSUOBJ=${NSUSRC:.c=.o}

OBJS=$(NSUOBJ) $(APPOBJ)

nsu: $(OBJS)
	cc $(CFLAGS) -o nsu $(OBJS) -lresolv -lnettle

$(NSUOBJ) $(APPOBJ): nsu.h

$(APPOBJ): nsu_app.h

y.tab.c y.tab.h: grammar.y
	yacc -dtv grammar.y
lex.yy.o: y.tab.h
lex.yy.c: lexer.l
	flex -d lexer.l

clean:
	rm -f nsu $(OBJS)

allclean: clean
	rm -f lex.yy.c y.tab.c y.tab.h

ETAGS     = etags
ETAGSFLAGS=

tags:
	$(ETAGS) $(ETAGSFLAGS) $(NSUSRC) main.c grammar.y lexer.l
