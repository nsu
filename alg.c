#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <nettle/md5.h>
#include <nettle/sha1.h>
#include <nettle/sha2.h>
#include <nettle/hmac.h>
#include <nettle/base64.h>
#include "nsu.h"

struct nsu_tsig_alg {
	char *name;
	int code;
	size_t digest_size;
	size_t ctx_size;
	const struct nettle_hash *nhash;
};

char const *
nsu_tsig_alg_name(struct nsu_tsig_alg const *alg)
{
	return alg->name;
}

size_t
nsu_tsig_alg_digest_size(struct nsu_tsig_alg const *alg)
{
	return alg->digest_size;
}

enum { CTX_INNER, CTX_OUTER, CTX_STATE };

static inline void *
nsu_tsig_ctx(struct nsu_tsig *sgn, int n)
{
	return sgn->ctx + n * sgn->alg->ctx_size;
}

void
nsu_tsig_set_key(struct nsu_tsig *sgn, u_char const *key, size_t len)
{
	hmac_set_key(nsu_tsig_ctx(sgn, CTX_OUTER),
		     nsu_tsig_ctx(sgn, CTX_INNER),
		     nsu_tsig_ctx(sgn, CTX_STATE),
		     sgn->alg->nhash,
		     len, key);
}
	
void
nsu_tsig_update(struct nsu_tsig *sgn, u_char const *data, size_t len)
{
	hmac_update(nsu_tsig_ctx(sgn, CTX_STATE), sgn->alg->nhash, len, data);
}

void
nsu_tsig_digest(struct nsu_tsig *sgn, u_char *data, size_t len)
{
	hmac_digest(nsu_tsig_ctx(sgn, CTX_OUTER),
		    nsu_tsig_ctx(sgn, CTX_INNER),
		    nsu_tsig_ctx(sgn, CTX_STATE),
		    sgn->alg->nhash,
		    len, data);
}

#define CODE_END 0
#define CODE_ALIAS (-1)

static struct nsu_tsig_alg algtab[] = {
	{ .name = "HMAC-MD5.SIG-ALG.REG.INT",
	  .code = 157,
	  .digest_size = MD5_DIGEST_SIZE,
	  .ctx_size = sizeof(struct md5_ctx),
	  .nhash = &nettle_md5,
	},
	{ .name = "HMAC-MD5",
	  .code = CODE_ALIAS,
	},
	{ .name = "HMAC-SHA1",
	  .code = 161,
	  .digest_size = SHA1_DIGEST_SIZE,
	  .nhash = &nettle_sha1,
	  .ctx_size = sizeof(struct sha1_ctx),
	},
	{ .name = "HMAC-SHA",
	  .code = CODE_ALIAS
	},
	{ .name = "HMAC-SHA224",
	  .code = 162,
	  .digest_size = SHA224_DIGEST_SIZE,
	  .nhash = &nettle_sha224,
	  .ctx_size = sizeof(struct sha224_ctx),
	},
	{ .name = "HMAC-SHA256",
	  .code = 163,
	  .digest_size = SHA256_DIGEST_SIZE,
	  .nhash = &nettle_sha256,
	  .ctx_size = sizeof(struct sha256_ctx),
	},
	{ .name = "HMAC-SHA384",
	  .code = 164,
	  .digest_size = SHA384_DIGEST_SIZE,
	  .nhash = &nettle_sha384,
	  .ctx_size = sizeof(struct sha384_ctx),
	},
	{ .name = "HMAC-SHA512",
	  .code = 165,
	  .digest_size = SHA512_DIGEST_SIZE,
	  .nhash = &nettle_sha512,
	  .ctx_size = sizeof(struct sha512_ctx),
	},
	{ .code = CODE_END }
};

struct nsu_tsig_alg const *
nsu_tsig_alg_find_code(int code)
{
	struct nsu_tsig_alg *ap;
	for (ap = algtab; ap->code != CODE_END; ap++)
		if (ap->code == code)
			return ap;
	return NULL;
}

struct nsu_tsig_alg const *
nsu_tsig_alg_find_name(char const *name)
{
	struct nsu_tsig_alg *ap;

	for (ap = algtab; ap->code == CODE_END; ap++) {
		if (strcasecmp(ap->name, name) == 0)
			break;
	}

	while (ap->code == CODE_ALIAS && ap > algtab)
		ap--;

	if (ap->code <= 0)
		return NULL;

	return ap;
}

struct nsu_tsig *
nsu_tsig_create(struct nsu_tsig_alg const *alg, char const *keyname)
{
	struct nsu_tsig *sgn;

	sgn = calloc(1, sizeof(*sgn));
	if (!sgn)
		return NULL;
	if ((sgn->name = strdup(keyname)) == NULL ||
	    (sgn->ctx = malloc(3 * alg->ctx_size)) == NULL ||
	    (sgn->digest = malloc(nsu_tsig_alg_digest_size(alg))) == NULL) {
		nsu_tsig_free(sgn);
		return NULL;
	}
	sgn->alg = alg;
	sgn->fudge = 300; //FIXME
	return sgn;
}

void
nsu_tsig_free(struct nsu_tsig *sgn)
{
	if (!sgn)
		return;
	free(sgn->name);
	free(sgn->ctx);
	free(sgn->digest);
	free(sgn);
}
/*
 * Read bind9 keyfile.
 */

struct filebuf {
	char const *name;
	unsigned line;
	int eof;
	FILE *file;
	char *buf;
	size_t bufsize;
	size_t buflen;

	char *keyname;
	struct nsu_tsig_alg const *alg;
	uint8_t *key;
	size_t key_len;
};

static void
filebuf_free(struct filebuf *fb)
{
	fclose(fb->file);
	free(fb->buf);
	free(fb->keyname);
	free(fb->key);
}

static int
filebuf_expand(struct filebuf *fb)
{
	size_t n = fb->bufsize;
	char *p;
	
	if (n == 0) {
		n = 64;
	} else if ((size_t) -1 / 3 * 2 <= n) {
		errno = ENOMEM;
		return -1;
	}
	p = realloc(fb->buf, n);
	if (!p) {
		errno = ENOMEM;
		return -1;
	}
	fb->buf = p;
	fb->bufsize = n;
	return 0;
}

static int
filebuf_getc(struct filebuf *fb)
{
	int c = fgetc(fb->file);
	switch (c) {
	case '\n':
		fb->line++;
		break;
	case EOF:
		fb->eof = 1;
	}
	return c;
}

static int
filebuf_addc(struct filebuf *fb, int c)
{
	if (fb->buflen == fb->bufsize && filebuf_expand(fb))
		return -1;
	fb->buf[fb->buflen++] = c;
	return 0;
}

/*
 * Skip whitespace on input.  Return first non-whitespace character read.
 * This can be EOF.
 */
static int
filebuf_skipws(struct filebuf *fb)
{
	int c;

	do
		c = filebuf_getc(fb);
	while (c != EOF && isspace(c));
	return c;
}

enum {
	TOK_NOMEM = -1,
	TOK_EOF = 0,
	TOK_ERR = 256,
	TOK_KEY,
	TOK_ALGO,
	TOK_SECRET,
	TOK_STRING,
};

static char *tokname[] = {
	[TOK_KEY] = "key",
	[TOK_ALGO] = "algorithm",
	[TOK_SECRET] = "secret",
	[TOK_STRING] = "quoted string"
};

static inline int
is_delim(int c)
{
	return isspace(c) || c == '{' || c == '}' || c == ';';
}

static int
filebuf_gettkn(struct filebuf *fb)
{
	int c;

	if (fb->eof)
		return TOK_EOF;
	fb->buflen = 0;
	while (1) {
		c = filebuf_skipws(fb);
		if (c == EOF)
			return TOK_EOF;
		if (c == '#') {
			while ((c = filebuf_getc(fb)) != EOF && c != '\n')
				;
			if (c == EOF)
				return TOK_EOF;
			continue;
		}
		break;
	}
	if (c == '"') {
		while ((c = filebuf_getc(fb)) != EOF && c != '"') {
			if (filebuf_addc(fb, c))
				return TOK_NOMEM;
		}
		if (c == EOF) {
 			nsu_log(NSU_LOG_ERR,
				"%s:%d: unexpected EOF in quoted string",
				fb->name,
				fb->line);
			return TOK_EOF;
		}
		return TOK_STRING;
	} else {
		do {
			if (filebuf_addc(fb, c))
				return TOK_NOMEM;
		} while ((c = filebuf_getc(fb)) != EOF && !is_delim(c));
		if (c != EOF)
			ungetc(c, fb->file);
		if (fb->buflen == 1)
			return fb->buf[0];
		if (fb->buflen == 3 && memcmp(fb->buf, "key", fb->buflen) == 0)
			return TOK_KEY;
		if (fb->buflen == 9 && memcmp(fb->buf, "algorithm", fb->buflen) == 0)
			return TOK_ALGO;
		if (fb->buflen == 6 && memcmp(fb->buf, "secret", fb->buflen) == 0)
			return TOK_SECRET;
		return TOK_STRING;
	}
	return TOK_ERR;
}

static char const *
tok_to_string(int tok)
{
	switch (tok) {
	case ';':
		return ";";
	case '{':
		return "{";
	case '}':
		return "}";
	}
	if (tok >= 0 && tok < sizeof(tokname) / sizeof(tokname[0]))
		return tokname[tok];
	return NULL;
}

static void
filebuf_perror(struct filebuf *fb, int tok, int exp)
{
	char const *expstr;
	switch (tok) {
	case TOK_EOF:
		nsu_log(NSU_LOG_ERR,
			"%s:%d: expected end of file",
			fb->name,
			fb->line);
		break;
		
	case TOK_NOMEM:
		nsu_log(NSU_LOG_ERR,
			"%s:%d: out of memory",
			fb->name,
			fb->line);
		break;

	case TOK_ERR:
		expstr = tok_to_string(exp);
		if (expstr)
			nsu_log(NSU_LOG_ERR,
				"%s:%d: expected %s, but found \"%*.*s\"",
				fb->name,
				fb->line,
				expstr,
				(int)fb->buflen,
				(int)fb->buflen,
				fb->buf);
		else
			nsu_log(NSU_LOG_ERR,
				"%s:%d: unexpected token \"%*.*s\"",
				fb->name,
				fb->line,
				(int)fb->buflen,
				(int)fb->buflen,
				fb->buf);
		break;

	case TOK_KEY:
	case TOK_ALGO:
	case TOK_SECRET:
	case TOK_STRING:
		expstr = tok_to_string(exp);
		if (expstr)
			nsu_log(NSU_LOG_ERR,
				"%s:%d: expected %s, but found %s",
				fb->name,
				fb->line,
				expstr,
				tokname[tok]);
		else
			nsu_log(NSU_LOG_ERR,
				"%s:%d: unexpected %s",
				fb->name,
				fb->line,
				tokname[tok]);
		break;

	default:
		abort();
	}		
}		

static int
filebuf_algo(struct filebuf *fb)
{
	int tok;
	
	tok = filebuf_gettkn(fb);
	if (tok != TOK_STRING) {
		filebuf_perror(fb, tok, TOK_STRING);
		return NSU_KEYFILE_BAD;
	}
	if (filebuf_addc(fb, 0))
		return NSU_KEYFILE_ERR;
	if ((fb->alg = nsu_tsig_alg_find_name(fb->buf)) == NULL) {
		nsu_log(NSU_LOG_ERR,
			"%s:%d: unknown or unsupported algorithm: %s",
			fb->name,
			fb->line,
			fb->buf);
		return NSU_KEYFILE_ALG;
	}
	
	return NSU_KEYFILE_OK;
}

static int
filebuf_secret(struct filebuf *fb)
{
	int tok;
	struct base64_decode_ctx ctx;
	
	tok = filebuf_gettkn(fb);
	if (tok != TOK_STRING) {
		filebuf_perror(fb, tok, TOK_STRING);
		return NSU_KEYFILE_BAD;
	}

	fb->key_len = BASE64_DECODE_LENGTH(fb->buflen);
	fb->key = malloc(fb->key_len);
	if (!fb->key)
		return NSU_KEYFILE_ERR;
			
	base64_decode_init(&ctx);
	base64_decode_update(&ctx, &fb->key_len, fb->key, fb->buflen,
			     (uint8_t*) fb->buf);
	if (!base64_decode_final(&ctx)) {
		nsu_log(NSU_LOG_ERR,
			"%s:%d: bad base64",
			fb->name, fb->line);
		return NSU_KEYFILE_BAD;
	}
	return NSU_KEYFILE_OK;
}

static int
parse_key(struct filebuf *fb, struct nsu_tsig **ptsig, int *try_compat)
{
	int tok;
	struct nsu_tsig *tsig;
	
	tok = filebuf_gettkn(fb);
	if (tok != TOK_KEY) {
		if (tok < TOK_ERR)
			return NSU_KEYFILE_ERR;
		if (try_compat)
			*try_compat = 1;
		else
			filebuf_perror(fb, tok, TOK_KEY);
		return NSU_KEYFILE_BAD;
	}

	if (try_compat)
		*try_compat = 0;

	tok = filebuf_gettkn(fb);
	if (tok != TOK_STRING) {
		filebuf_perror(fb, tok, TOK_STRING);
		return tok < TOK_ERR ? NSU_KEYFILE_ERR : NSU_KEYFILE_BAD;
	}
	fb->keyname = malloc(fb->buflen+1);
	if (!fb->keyname) {
		filebuf_perror(fb, TOK_NOMEM, TOK_ERR);
		return tok < TOK_ERR ? NSU_KEYFILE_ERR : NSU_KEYFILE_BAD;
	}
	memcpy(fb->keyname, fb->buf, fb->buflen);
	fb->keyname[fb->buflen] = 0;

	tok = filebuf_gettkn(fb);
	if (tok != '{') {
		filebuf_perror(fb, tok, '{');
		return NSU_KEYFILE_BAD;
	}

	while (1) {
		int rc;
		
		tok = filebuf_gettkn(fb);
		if (tok == TOK_ALGO) {
			if ((rc = filebuf_algo(fb)) != NSU_KEYFILE_OK)
				return rc;
		} else if (tok == TOK_SECRET) {
			if ((rc = filebuf_secret(fb)) != NSU_KEYFILE_OK)
				return rc;
		} else if (tok == '}')
			break;
		else {
			filebuf_perror(fb, tok, TOK_ERR);
			return tok < TOK_ERR ? NSU_KEYFILE_ERR : NSU_KEYFILE_BAD;
		}

		tok = filebuf_gettkn(fb);
		if (tok != ';') {
			filebuf_perror(fb, tok, ';');
			return tok < TOK_ERR ? NSU_KEYFILE_ERR : NSU_KEYFILE_BAD;
	        }
	}
	
	tsig = nsu_tsig_create(fb->alg, fb->keyname);
	if (!tsig)
		return NSU_KEYFILE_ERR;
	else
		nsu_tsig_set_key(tsig, fb->key, fb->key_len);

	nsu_log(NSU_LOG_DEBUG, "%s: algo=%s, keyname=%s, key length=%zu",
		fb->name, fb->alg->name, fb->keyname, fb->key_len);

	*ptsig = tsig;

	return NSU_KEYFILE_OK;
}
	
int
nsu_keyfile_read_new(char const *name, struct nsu_tsig **ptsig, int *try_compat)
{
	struct filebuf fbuf;
	int rc;
	
	memset(&fbuf, 0, sizeof(fbuf));
	fbuf.file = fopen(name, "r");
	if (!fbuf.file) {
		if (errno == ENOENT && try_compat) {
		        *try_compat = 1;   
	        } else {
		        nsu_log(NSU_LOG_ERR, "can't open %s: %s",
			        name, strerror(errno));
	        }
		return NSU_KEYFILE_ERR;
	}
	fbuf.name = name;
	fbuf.line = 1;
	nsu_log(NSU_LOG_DEBUG, "Parsing keyfile %s", name);

	rc = parse_key(&fbuf, ptsig, try_compat);

	filebuf_free(&fbuf);
	return rc;
}

/* Parse the keyfile name and attempt to extract the keyname from it.
 *
 * The FILENAME is expected to match the following pattern:
 *
 *      K<KEYNAME>.+<ALGCODE>+<NUMBER>.<EXT>
 *
 * where <ALGCODE> and <NUMBER> are decimal numbers, <EXT> is any string,
 * and <ALGCODE> equals ALG.  If these conditions are met, the <KEYNAME>
 * is copied to the allocated memory block and the address of that block
 * is stored in KEYNAME.
 *
 * Return value: 0 - success; KEYNAME holds the key name.
 *               1 - FILENAME doesn't match the pattern.
 *              -1 - system error; inspect errno for details.
 */
static int
filename_to_keyname(char const *filename, int alg, char **keyname)
{
	char const *start, *p, *q;
	char *key;
	size_t len;
	
	p = strrchr(filename, '/');
	if (p)
		filename = p + 1;

	if (filename[0] != 'K')
		return 1;
	filename++;
	start = filename;
	len = strcspn(start, "+");
	if (len < 2 || start[len] != '+' || start[len-1] != '.')
		return 1;
	filename += len + 1;
	
	p = strchr(filename, '+');
	if (!p)
		return 1;
	for (q = p + 1; *q && *q != '.'; q++) {
		if (!isdigit(*q))
			return 1;
	}

	q = p;
	while (alg > 0) {
		if (--q == p)
			return 1;
		else if (alg % 10 + '0' != *q)
			return 1;
		alg /= 10;
	}

	key = malloc(len);
	if (!key)
		return -1;
	len--;
	memcpy(key, start, len);
	key[len] = 0;
	
	*keyname = key;
	return 0;
}

static int
filename_to_alg(char const *filename, struct nsu_tsig_alg const **palg)
{
	long n;
	char const *p, *q;
	char *end;
	size_t len;

	p = strrchr(filename, '/');
	if (p)
		filename = p + 1;
	
	if (filename[0] != 'K')
		return 1;
	filename++;
	len = strcspn(filename, "+");
	if (len < 2 || filename[len] != '+' || filename[len-1] != '.')
		return 1;
	filename += len + 1;
	
	p = strchr(filename, '+');
	if (!p)
		return 1;
	for (q = p + 1; *q && *q != '.'; q++) {
		if (!isdigit(*q))
			return 1;
	}
	errno = 0;
	n = strtol(filename, &end, 10);
	if (errno || *end != '+')
		return 1;

	*palg = nsu_tsig_alg_find_code(n);
	return 0;
}

/* Parse the private key file generated by dnssec-keygen.
 * On success, fill the return arguments:
 *
 *  palg     - pointer to the nsu_tsig_alg structure;
 *  pkeyname - (malloc) Key name or NULL if unable to deduce;
 *  pkey     - (malloc) Key value.
 *  pkey_len - length of the key.
 *
 * Arguments marked with (malloc) are allocated.
 *
 * Return value:
 *  NSU_KEYFILE_OK  - success;
 *  NSU_KEYFILE_BAD - bad file format;
 *  NSU_KEYFILE_ALG - file refers to an unknown or unsupported algorithm;
 *  NSU_KEYFILE_ERR - system error; inspect errno;
 */
int
nsu_keyfile_private_read(char const *filename,
			 struct nsu_tsig_alg const **palg,
			 char **pkeyname,
			 uint8_t **pkey, size_t *pkey_len)
{
	FILE *fp;
	char buf[512];
	int lno = 0;
	int rc = NSU_KEYFILE_OK;
	struct nsu_tsig_alg const *alg = NULL;
	uint8_t *key = NULL;
	size_t key_len;
	char *keyname = NULL;
	
	static char algorithm_field[] = "Algorithm";
	static int algorithm_length = sizeof(algorithm_field) - 1;
	static char key_field[] = "Key";
	static int key_length = sizeof(key_field) - 1;

	fp = fopen(filename, "r");
	if (!fp) {
		nsu_log(NSU_LOG_ERR, "can't open %s: %s",
			filename, strerror(errno));
		rc = NSU_KEYFILE_ERR;
	} else {
		nsu_log(NSU_LOG_DEBUG, "Parsing private keyfile %s", filename);
		while (fgets(buf, sizeof(buf), fp)) {
			size_t len, n;
			lno++;
			len = strlen(buf);
			if (len == 0)
				break;
			if (buf[len-1] != '\n') {
				nsu_log(NSU_LOG_ERR, "%s:%d: line too long",
					filename, lno);
				rc = NSU_KEYFILE_BAD;
				break;
			}
			buf[--len] = '\n';
			n = strcspn(buf, ":");
			if (buf[n] == 0) {
				nsu_log(NSU_LOG_ERR, "%s:%d: malformed line",
					filename, lno);
				rc = NSU_KEYFILE_BAD;
				break;
			}
			if (n == algorithm_length &&
			    memcmp(algorithm_field, buf, n) == 0) {
				char *p;
				long code;
				errno = 0;
				code = strtol(buf + n + 1, &p, 10);
				if (errno || *p != ' ') {
					nsu_log(NSU_LOG_ERR,
						"%s:%d: malformed line",
						filename, lno);
					rc = NSU_KEYFILE_BAD;
					break;
				}
			
				alg = nsu_tsig_alg_find_code(code);
				if (!alg) {
					nsu_log(NSU_LOG_ERR,
						"%s:%d: unsupported algorithm %s",
						filename, lno, buf + n);
					rc = NSU_KEYFILE_ALG;
					break;
				}
			} else if (n == key_length &&
				   memcmp(key_field, buf, n) == 0) {
				struct base64_decode_ctx ctx;
				size_t src_len;
				char *p;

				p = buf + n + 1;
				while (*p == ' ' || *p == '\t')
					p++;
				src_len = strlen(p);
				key_len = BASE64_DECODE_LENGTH(src_len);
				key = malloc(key_len);
				if (!key) {
					rc = NSU_KEYFILE_ERR;
					break;
				}
			
				base64_decode_init(&ctx);
				base64_decode_update(&ctx, &key_len, key,
						     src_len, (uint8_t*) p);
				if (!base64_decode_final(&ctx)) {
					nsu_log(NSU_LOG_ERR,
						"%s:%d: bad base64",
						filename, lno);
					rc = NSU_KEYFILE_BAD;
					break;
				}
			}
		}
		fclose(fp);
	}
	
	if (rc == NSU_KEYFILE_OK) {
		if (!alg) {
			nsu_log(NSU_LOG_ERR, "%s: no %s clause",
				filename, algorithm_field);
			rc = NSU_KEYFILE_BAD;
		}
		if (!key) {
			nsu_log(NSU_LOG_ERR, "%s: no %s clause",
				filename, key_field);
			rc = NSU_KEYFILE_BAD;
		}
	}
	
	if (rc == NSU_KEYFILE_OK) {
		char const *basename = strrchr(filename, '/');
		if (basename)
			basename++;
		else
			basename = filename;
		if (filename_to_keyname(filename, alg->code, &keyname) < 0)
			rc = NSU_KEYFILE_ERR;
	}
	
	if (rc == NSU_KEYFILE_OK) {
		nsu_log(NSU_LOG_DEBUG, "%s: algo=%s, keyname=%s, key length=%zu",
			filename, alg->name, keyname ? keyname : "(unknown)",
			key_len);
		*pkeyname = keyname;
		*palg = alg;
		*pkey = key;
		*pkey_len = key_len;
	} else {
		free(key);
		free(keyname);
	}
	
	return rc;
}

/* Parse the private key file generated by dnssec-keygen.
 * On success, fill the return arguments:
 *
 *  pkeyname - (malloc) Key name or NULL if unable to deduce;
 *  pkey     - (malloc) Key value.
 *  pkey_len - length of the key.
 *
 * Arguments marked with (malloc) are allocated.
 *
 * Return value: see nsu_keyfile_private_read
 */
int
nsu_keyfile_key_read(char const *filename,
		     struct nsu_tsig_alg const **palg,
		     char **pkeyname,
		     uint8_t **pkey, size_t *pkey_len)
{
	char buf[512];
	char *ret, *keyptr;
	size_t n;
	uint8_t *key = NULL;
	FILE *fp;
	int i;
	size_t src_len, key_len;
	struct base64_decode_ctx ctx;
	int rc;
	
	fp = fopen(filename, "r");
	if (!fp) {
		nsu_log(NSU_LOG_ERR, "can't open %s: %s",
			filename, strerror(errno));
		rc = NSU_KEYFILE_ERR;
		goto err;
	}
	
	nsu_log(NSU_LOG_DEBUG, "Parsing keyfile %s", filename);
	if (!fgets(buf, sizeof(buf), fp)) {
		nsu_log(NSU_LOG_ERR, "%s: read error", filename);
		rc = NSU_KEYFILE_ERR;
		goto err;
	}
		
	n = strcspn(buf, " ");
	if (strncmp(buf + n, " IN KEY", 7)) {
		nsu_log(NSU_LOG_ERR, "%s:%d: malformed line", filename, 1);
		rc = NSU_KEYFILE_BAD;
		goto err;
	}

	keyptr = buf + n + 7;
	for (i = 0; i < 3; i++) {
		while (*keyptr && isspace(*keyptr))
			keyptr++;
		while (*keyptr && isdigit(*keyptr))
			keyptr++;
	}
	while (*keyptr && isspace(*keyptr))
		keyptr++;

	src_len = strlen(keyptr);
	key_len = BASE64_DECODE_LENGTH(src_len);
	key = malloc(key_len);
	if (!key) {
		rc = NSU_KEYFILE_ERR;
		goto err;
	}
			
	base64_decode_init(&ctx);
	base64_decode_update(&ctx, &key_len, key, src_len, (uint8_t*) keyptr);
	if (!base64_decode_final(&ctx)) {
		nsu_log(NSU_LOG_ERR, "%s:%d: bad base64", filename, 1);
		rc = NSU_KEYFILE_BAD;
		goto err;
	}

	if (buf[n-1] == '.')
		--n;
	ret = malloc(n+1);
	if (!ret) {
		rc = NSU_KEYFILE_ERR;
		goto err;
	}
	memcpy(ret, buf, n);
	ret[n] = 0;
	
	filename_to_alg(filename, palg);
	*pkeyname = ret;
	*pkey = key;
	*pkey_len = key_len;
	nsu_log(NSU_LOG_DEBUG, "%s: algo=%s, keyname=%s, key length=%zu",
		filename,
		*palg ? (*palg)->name : "(unknown)",
		ret, key_len);
	rc = NSU_KEYFILE_OK;
err:
	if (rc != NSU_KEYFILE_OK)
		free(key);
		
	fclose(fp);
		
	return rc;
}

static int
is_suffix(char const *suf, char const *str)
{
	size_t suflen = strlen(suf);
	size_t len = strlen(str);
	return suflen < len && memcmp(str + len - suflen, suf, suflen) == 0;
}

static char const *priv_suffix = ".private";
static char const *key_suffix = ".key";

/*
 * Create new file name by removing last SUFLEN characters from BASENAME
 * and appending NEWSUF to the resulting string.  Returned value is
 * malloced.
 */
static char *
makefilename(char const *basename, size_t suflen, char const *new_suf)
{
	size_t len = strlen(basename);
	char *p;
	
	if (len <= suflen) {
		errno = EINVAL;
		return NULL;
	}

	len -= suflen;
	p = malloc(len + strlen(new_suf) + 1);
	if (!p)
		return p;

	memcpy(p, basename, len);
	strcpy(p + len, new_suf);

	return p;
}

/*
 * Read the keyfile NAME.  On success, create struct nsu_tsig and
 * populate it with the information obtained from the file.
 *
 * Return NSU_KEYFILE_OK on success, and another NSU_KEYFILE_* constant
 * on error (see the description in nsu_keyfile_private_read).
 *
 * If NAME ends with ".private", that file is read.  If the
 * key name cannot be deduced from the file name, the ".key" file
 * will be read in addition, in order to determine it.
 *
 * If NAME ends with dot, then first the dot is replaced with
 * ".private" and an attempt is made to read that file as described
 * above.  If the private file does not exist, the suffix is replaced
 * with ".key" and the file is read as described below.
 *
 * If NAME ends with ".key", the key file is read.  The algorithm
 * is then deduced from the file name.
 */
int
nsu_keyfile_read_old(char const *name, struct nsu_tsig **ptsig)
{
	struct nsu_tsig_alg const *alg = NULL, *kalg;
	uint8_t *key, *kkey;
	size_t key_len, kkey_len;
	char *keyname = NULL;
	struct nsu_tsig *tsig;
	int rc;
	char *keyfilename = NULL;
	
	if (is_suffix(priv_suffix, name)) {
		rc = nsu_keyfile_private_read(name, &alg, &keyname,
					      &key, &key_len);
		switch (rc) {
		case NSU_KEYFILE_OK:
			if (keyname)
				name = NULL;
			else {
				keyfilename = makefilename(name,
							   strlen(priv_suffix),
							   key_suffix);
				if (!keyfilename)
					return NSU_KEYFILE_ERR;
				name = keyfilename;
				nsu_log(NSU_LOG_DEBUG,
					"keyname not known, will try %s", name);
			}
			break;
			
		case NSU_KEYFILE_BAD:
		case NSU_KEYFILE_ALG:
		case NSU_KEYFILE_ERR:
			return rc;
		}
	} else if (is_suffix(".", name)) {
		char *tmpname = makefilename(name, 1, priv_suffix);
		if (!tmpname)
			return NSU_KEYFILE_ERR;
		rc = nsu_keyfile_private_read(tmpname,
					      &alg, &keyname, &key, &key_len);
		free(tmpname);
		switch (rc) {
		case NSU_KEYFILE_OK:
			if (keyname)
				name = NULL;
			else {
				keyfilename = makefilename(name,
							   1,
							   key_suffix);
				if (!keyfilename)
					return NSU_KEYFILE_ERR;
				name = keyfilename;
				nsu_log(NSU_LOG_DEBUG,
					"keyname not known, will try %s", name);
			}
			break;
			
		case NSU_KEYFILE_BAD:
		case NSU_KEYFILE_ALG:
			return rc;

		case NSU_KEYFILE_ERR:
			if (errno == ENOENT) {
				keyfilename = makefilename(name,
							   1,
							   key_suffix);
				if (!keyfilename)
					return NSU_KEYFILE_ERR;
				nsu_log(NSU_LOG_DEBUG,
					"file %s does not exist, "
					"will try %s", name, keyfilename);
				name = keyfilename;
			} else
				return rc;
		}
	} else if (!is_suffix(key_suffix, name)) {
		nsu_log(NSU_LOG_DEBUG,
			"%s: unknown keyfile suffix", name);
		return NSU_KEYFILE_BAD;
	}
	
	if (name) {
		rc = nsu_keyfile_key_read(name, &kalg, &keyname,
					  &kkey, &kkey_len);

		if (rc == NSU_KEYFILE_OK) {
			if (!alg)
				alg = kalg;
			else if (kalg != alg) {
				nsu_log(NSU_LOG_ERR, "%s: algorithm mismatch",
					name);
				rc = NSU_KEYFILE_BAD;
			}
		}
	
		if (rc == NSU_KEYFILE_OK) { 
			if (!key) {
				key = kkey;
				key_len = kkey_len;
				kkey = NULL;
			} else if (kkey_len != key_len ||
				   memcmp(kkey, key, key_len)) {
				nsu_log(NSU_LOG_ERR, "%s: keys don't match",
					name);
				rc = NSU_KEYFILE_BAD;
			}
		}
		free(kkey);
		free(keyfilename);
	}

	if (rc == NSU_KEYFILE_OK) {
		tsig = nsu_tsig_create(alg, keyname);
		if (!tsig)
			rc = NSU_KEYFILE_ERR;
		else
			nsu_tsig_set_key(tsig, key, key_len);
	}
	
	free(keyname);
	free(key);	

	if (rc == NSU_KEYFILE_OK)
		*ptsig = tsig;
	
	return rc;
}

int
nsu_keyfile_read(char const *name, struct nsu_tsig **ptsig)
{
	int rc;
	int try_compat;

	rc = nsu_keyfile_read_new(name, ptsig, &try_compat);
	if (rc == NSU_KEYFILE_OK)
		return rc;
	else if (try_compat)
		rc = nsu_keyfile_read_old(name, ptsig);
	return rc;
}



