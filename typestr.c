#include <resolv.h>
#include <stdlib.h>
#include <string.h>
#include "nsu.h"

static char *nstypestr[] = {
	[ns_t_a]        = "A",     
	[ns_t_ns]       = "NS",    
	[ns_t_md]       = "MD",    
	[ns_t_mf]       = "MF",    
	[ns_t_cname]    = "CNAME", 
	[ns_t_soa]      = "SOA",   
	[ns_t_mb]       = "MB",    
	[ns_t_mg]       = "MG",    
	[ns_t_mr]       = "MR",    
	[ns_t_null]     = "NULL",  
	[ns_t_wks]      = "WKS",   
	[ns_t_ptr]      = "PTR",   
	[ns_t_hinfo]    = "HINFO", 
	[ns_t_minfo]    = "MINFO", 
	[ns_t_mx]       = "MX",    
	[ns_t_txt]      = "TXT",   
	[ns_t_rp]       = "RP",    
	[ns_t_afsdb]    = "AFSDB", 
	[ns_t_x25]      = "X25",   
	[ns_t_isdn]     = "ISDN",  
	[ns_t_rt]       = "RT",    
	[ns_t_nsap]     = "NSAP",  
	[ns_t_sig]      = "SIG",   
	[ns_t_key]      = "KEY",   
	[ns_t_px]       = "PX",    
	[ns_t_aaaa]     = "AAAA",  
	[ns_t_loc]      = "LOC",   
	[ns_t_nxt]      = "NXT",   
	[ns_t_eid]      = "EID",   
	[ns_t_nimloc]   = "NIMLOC",
	[ns_t_srv]      = "SRV",   
	[ns_t_atma]     = "ATMA",  
	[ns_t_naptr]    = "NAPTR", 
	[ns_t_kx]       = "KX",    
	[ns_t_cert]     = "CERT",  
	[ns_t_dname]    = "DNAME", 
	[ns_t_sink]     = "SINK",  
	[ns_t_opt]      = "OPT",   
	[ns_t_apl]      = "APL",   
	[ns_t_tkey]     = "TKEY",  
	[ns_t_tsig]     = "TSIG"   
};

int
nsu_str_to_type(char const *str)
{
	int i;
	for (i = 0; i < sizeof(nstypestr)/sizeof(nstypestr[0]); i++)
		if (nstypestr[i] && strcasecmp(nstypestr[i], str) == 0)
			return i;
	if (strncmp(str, "TYPE", 4) == 0) {
		char *p;
		unsigned long n;
		n = strtoul(str + 4, &p, 10);
		if (n != ULONG_MAX && *p == 0 && n < ns_t_max)
			return n;
	}
	return ns_t_invalid;
}

char const *
nsu_type_to_str(int type)
{
	if (type < sizeof(nstypestr)/sizeof(nstypestr[0]))
		return nstypestr[type];
	return NULL;
}
